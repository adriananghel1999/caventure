﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Globalization;

public class BorrarUsuario : MonoBehaviour
{

    ListarPuntuaciones lp;

    public GameObject information, panelNotRegistered;

    void OnEnable()
    {
        lp = GameObject.Find("RankContent").GetComponent<ListarPuntuaciones>();
        information.GetComponent<Text>().text = "Are you sure you want to delete your account?";
    }

    public void tryBorrarUsuario()
    {
        StartCoroutine(borrarUsuario());
    }

    public IEnumerator borrarUsuario()
    {
        WWWForm form = new WWWForm();
        form.AddField("Nickname", GlobalOptions.Instance.getNicknameActualSession());
        form.AddField("Password", Md5Sum(GlobalOptions.Instance.getPasswordActualSession()));

        using (UnityWebRequest www = UnityWebRequest.Post("http://anghel.com.es/caventure/borrar_usuario.php", form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                information.GetComponent<Text>().text = "Server error";
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Error servidor")
                {
                    information.GetComponent<Text>().color = Color.red;
                    information.GetComponent<Text>().text = "Connection error, try again.";
                }

                if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Falta field")
                {
                    information.GetComponent<Text>().color = Color.red;
                    information.GetComponent<Text>().text = "Server error, try again.";
                }

                if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Usuario borrado")
                {

                    GlobalOptions.Instance.setNicknameActualSession("");
                    GlobalOptions.Instance.setPasswordActualSession("");

                    if (GlobalOptions.Instance.rememberSession == true)
                    {
                        GlobalOptions.Instance.saveSession();
                    }

                    panelNotRegistered.SetActive(true);
                    gameObject.SetActive(false);
                    lp.meterPuntuaciones();

                }
            }
        }

    }

    [System.Serializable]
    public class messageJson
    {
        public string message;
    }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }


}
