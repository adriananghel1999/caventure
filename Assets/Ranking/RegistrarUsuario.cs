﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Globalization;

public class RegistrarUsuario : MonoBehaviour
{

    ListarPuntuaciones lp;

    public GameObject nickname, password, repeatPassword, information;

    // Start is called before the first frame update
    void OnEnable()
    {
        lp = GameObject.Find("RankContent").GetComponent<ListarPuntuaciones>();

        nickname.GetComponent<InputField>().textComponent.text = "";
        password.GetComponent<InputField>().textComponent.text = "";
        repeatPassword.GetComponent<InputField>().textComponent.text = "";
        information.GetComponent<Text>().text = "";
    }

    public void tryRegistrarUsuario()
    {
        StartCoroutine(registrarUsuario());
    }

    public IEnumerator registrarUsuario()
    {
        if (Validator())
        {
            WWWForm form = new WWWForm();
            form.AddField("Nickname", nickname.GetComponent<InputField>().textComponent.text);
            form.AddField("Password", password.GetComponent<InputField>().text);

            using (UnityWebRequest www = UnityWebRequest.Post("http://anghel.com.es/caventure/registrar_usuario.php", form))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError)
                {
                    information.GetComponent<Text>().text = "Server error";
                    Debug.Log(www.error);
                }
                else
                {
                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Error")
                    {
                        information.GetComponent<Text>().color = Color.red;
                        information.GetComponent<Text>().text = "There's already an account with that nickname or can't connect with server.";
                    }

                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Exito al crear cuenta.")
                    {
                        StartCoroutine(lp.meterPuntuaciones());

                        information.GetComponent<Text>().color = Color.green;
                        information.GetComponent<Text>().text = "Your account has been registered.";

                        nickname.GetComponent<InputField>().text = "";
                        password.GetComponent<InputField>().text = "";
                        repeatPassword.GetComponent<InputField>().text = "";
                    }
                }
            }

        }
        else { }
    }

    [System.Serializable]
    public class messageJson
    {
        public string message;
    }

    public bool Validator()
    {

        if (nickname.GetComponent<InputField>().textComponent.text != "" && password.GetComponent<InputField>().textComponent.text != "" && repeatPassword.GetComponent<InputField>().textComponent.text != "")
        {
            if (password.GetComponent<InputField>().text == repeatPassword.GetComponent<InputField>().text)
            {

                return true;
            }
            else
            {
                information.GetComponent<Text>().text = "Password and repeat password field are not the same.";
                information.GetComponent<Text>().color = Color.red;
                return false;
            }
        }
        else
        {
            information.GetComponent<Text>().text = "One of the fields are empty";
            information.GetComponent<Text>().color = Color.red;
            return false;
        }
    }


}
