﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Globalization;

public class LoginUsuario : MonoBehaviour
{

    ListarPuntuaciones lp;

    public GameObject nickname, password, information, panelRegistered;

    // Start is called before the first frame update
    void OnEnable()
    {
        lp = GameObject.Find("RankContent").GetComponent<ListarPuntuaciones>();

        nickname.GetComponent<InputField>().textComponent.text = "";
        password.GetComponent<InputField>().textComponent.text = "";
        information.GetComponent<Text>().text = "";
    }

    public void tryloginUsuario()
    {
        StartCoroutine(loginUsuario());
    }

    public IEnumerator loginUsuario()
    {
        if (Validator())
        {
            WWWForm form = new WWWForm();
            form.AddField("Nickname", nickname.GetComponent<InputField>().textComponent.text);
            form.AddField("Password", Md5Sum(password.GetComponent<InputField>().text));

            using (UnityWebRequest www = UnityWebRequest.Post("http://anghel.com.es/caventure/login_usuario.php", form))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError)
                {
                    information.GetComponent<Text>().text = "Server error";
                    Debug.Log(www.error);
                }
                else
                {
                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Error servidor")
                    {
                        information.GetComponent<Text>().color = Color.red;
                        information.GetComponent<Text>().text = "Incorrect nickname/password.";
                    }

                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Falta field")
                    {
                        information.GetComponent<Text>().color = Color.red;
                        information.GetComponent<Text>().text = "One field is empty.";
                    }

                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Usuario encontrado")
                    {
                        GlobalOptions.Instance.setNicknameActualSession(nickname.GetComponent<InputField>().text);
                        GlobalOptions.Instance.setPasswordActualSession(password.GetComponent<InputField>().text);

                        if (GlobalOptions.Instance.rememberSession == true){
                            GlobalOptions.Instance.saveSession();
                        }

                        nickname.GetComponent<InputField>().text = "";
                        password.GetComponent<InputField>().text = "";

                        panelRegistered.SetActive(true);
                        gameObject.SetActive(false);
                    }
                }
            }

        }
        else { }
    }

    [System.Serializable]
    public class messageJson
    {
        public string message;
    }

    public bool Validator()
    {

        if (nickname.GetComponent<InputField>().textComponent.text != "" && password.GetComponent<InputField>().textComponent.text != "")
        {
            return true;
        }
        else
        {
            information.GetComponent<Text>().text = "One of the fields are empty";
            information.GetComponent<Text>().color = Color.red;
            return false;
        }
    }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }


}
