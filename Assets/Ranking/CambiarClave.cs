﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Globalization;

public class CambiarClave : MonoBehaviour
{


    public GameObject oldPassword, newPassword, repeatNewPassword, information;

    // Start is called before the first frame update
    void OnEnable()
    {

        oldPassword.GetComponent<InputField>().textComponent.text = "";
        newPassword.GetComponent<InputField>().textComponent.text = "";
        repeatNewPassword.GetComponent<InputField>().textComponent.text = "";
        information.GetComponent<Text>().text = "";
    }

    public void tryCambiarClave()
    {
        StartCoroutine(cambiarClave());
    }

    public IEnumerator cambiarClave()
    {
        if (Validator())
        {
            WWWForm form = new WWWForm();
            form.AddField("Nickname", GlobalOptions.Instance.getNicknameActualSession());
            form.AddField("OldPassword", oldPassword.GetComponent<InputField>().text);
            form.AddField("NewPassword", newPassword.GetComponent<InputField>().text);

            using (UnityWebRequest www = UnityWebRequest.Post("http://anghel.com.es/caventure/cambiar_clave.php", form))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError)
                {
                    information.GetComponent<Text>().text = "Server error";
                    Debug.Log(www.error);
                }
                else
                {
                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Error servidor")
                    {
                        information.GetComponent<Text>().color = Color.red;
                        information.GetComponent<Text>().text = "Error while sending information, try again.";
                    }

                    if (JsonUtility.FromJson<messageJson>(www.downloadHandler.text).message == "Clave cambiada")
                    {

                        information.GetComponent<Text>().color = Color.green;
                        information.GetComponent<Text>().text = "Your password has been changed.";

                        GlobalOptions.Instance.setPasswordActualSession(newPassword.GetComponent<InputField>().text);
                        GlobalOptions.Instance.saveSession();

                        oldPassword.GetComponent<InputField>().text = "";
                        newPassword.GetComponent<InputField>().text = "";
                        repeatNewPassword.GetComponent<InputField>().text = "";
                    }
                }
            }

        }
        else { }
    }

    [System.Serializable]
    public class messageJson
    {
        public string message;
    }

    public bool Validator()
    {

        if (oldPassword.GetComponent<InputField>().textComponent.text != "" && newPassword.GetComponent<InputField>().textComponent.text != "" && repeatNewPassword.GetComponent<InputField>().textComponent.text != "")
        {
            if (newPassword.GetComponent<InputField>().text == repeatNewPassword.GetComponent<InputField>().text)
            {

                return true;
            }
            else
            {
                information.GetComponent<Text>().text = "Password and repeat password field are not the same.";
                information.GetComponent<Text>().color = Color.red;
                return false;
            }
        }
        else
        {
            information.GetComponent<Text>().text = "One of the fields are empty";
            information.GetComponent<Text>().color = Color.red;
            return false;
        }
    }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }


}
