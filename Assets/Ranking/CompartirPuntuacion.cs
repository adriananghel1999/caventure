﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Globalization;

public class CompartirPuntuacion : MonoBehaviour
{

    public void tryCompartirPuntuacion()
    {
        StartCoroutine(cambiarPuntuacion());
    }

    public IEnumerator cambiarPuntuacion()
    {
        if (GlobalOptions.Instance.getNicknameActualSession() != "" && GlobalOptions.Instance.getPasswordActualSession() != "")
        {
            WWWForm form = new WWWForm();
            form.AddField("Nickname", GlobalOptions.Instance.getNicknameActualSession());
            form.AddField("Password", GlobalOptions.Instance.getPasswordActualSession());
            form.AddField("Puntuacion", GlobalControl.Instance.puntuacion);

            using (UnityWebRequest www = UnityWebRequest.Post("http://anghel.com.es/caventure/compartir_puntuacion.php", form))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {

                }
            }
        }
        else
        {

        }
    }

    [System.Serializable]
    public class messageJson
    {
        public string message;
    }


}
