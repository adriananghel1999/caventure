﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Globalization;

public class ListarPuntuaciones : MonoBehaviour
{
    public GameObject content;

    public ItemList Itemlist = new ItemList();

    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(meterPuntuaciones());
    }

    public IEnumerator meterPuntuaciones()
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        using (UnityWebRequest www = UnityWebRequest.Get("http://anghel.com.es/caventure/listar_ranking.php"))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            { // NO TOCAR NADA, ESTO FUNCIONA, ME HE TIRADO ALREDEDOR DE 10 HORAS INTENTADO HACER ESTO FUNCIONAR CON DOCUMENTACION Y DE REPENTE ME SALTA UN PAVO EN YOUTUBE CON 3 O 4 VERSIONES DE UNITY ANTERIORES A LA MIA Y QUE PARECE QUE ES DE ALBACETE Y ME LO SOLUCIONA
                Itemlist = JsonUtility.FromJson<ItemList>(www.downloadHandler.text);
                content.GetComponent<Text>().text = "Nickname - Score";
                Instantiate(content, transform.position, Quaternion.identity, gameObject.transform);
                foreach (Item item in Itemlist.item)
                {
                    content.GetComponent<Text>().text = item.nickname + " - " + item.puntuacion;
                    Instantiate(content, transform.position, Quaternion.identity, gameObject.transform);
                }
            }
        }
    }

    [System.Serializable]
    public class Item
    {
        public string nickname;
        public int puntuacion;
    }

    [System.Serializable]
    public class ItemList
    {
        public List<Item> item = new List<Item>();
    }

}
