﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Conexion : MonoBehaviour
{
    public GameObject rankingPanel;

    // Start is called before the first frame update
    void OnEnable()
    {

        StartCoroutine(TryConexion());
    }

    IEnumerator TryConexion()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://anghel.com.es/caventure/conexion.php"))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                rankingPanel.SetActive(true);
                gameObject.SetActive(false);

            }
        }
    }
}
