﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsSessionOn : MonoBehaviour
{
    public GameObject login, notLogin;

    void OnEnable()
    {

        if (GlobalOptions.Instance.getNicknameActualSession() != "" || GlobalOptions.Instance.getPasswordActualSession() != ""){
            login.SetActive(true);
        } else {
            notLogin.SetActive(true);
        }
    }

    public void logOut(){
        GlobalOptions.Instance.setNicknameActualSession("");
        GlobalOptions.Instance.setPasswordActualSession("");
    }


}
