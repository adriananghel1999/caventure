<?php

/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */

// array for JSON response
$response = array();

// include db connect class
require_once __DIR__ . '/conexion.php';

// check for required fields
if (isset($_POST['Nickname']) && isset($_POST['Password']) && isset($_POST['Puntuacion'])) {
    
    $Nickname = $_POST['Nickname'];
    $Password = $_POST['Password'];
    $Puntuacion = $_POST['Puntuacion'];

    $result = mysqli_query($conexion, "SELECT * FROM `usuarios` WHERE `nickname`= '$Nickname' and `clave` = MD5('$Password')");

    $row = mysqli_fetch_array($result);

    // check if row inserted or not
    if (empty($row)) {
		// failed to insert row
        $response["message"] = "Error servidor";
        
        // echoing JSON response
        echo json_encode($response);
        
    } else {
        $result = mysqli_query($conexion, "UPDATE `caventure`.`usuarios` SET `puntuacion` = GREATEST($Puntuacion, `puntuacion`) WHERE `usuarios`.`nickname` = '$Nickname'");

		// successfully inserted into database
        $response["message"] = "Puntuacion cambiada";

        // echoing JSON response
        echo json_encode($response);
		
        
    }
} else {
    // required field is missing
    $response["message"] = "Falta field";

    // echoing JSON response
    echo json_encode($response);
}

?>
