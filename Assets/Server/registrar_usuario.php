<?php

/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */

// array for JSON response
$response = array();

// include db connect class
require_once __DIR__ . '/conexion.php';

// check for required fields
if (isset($_POST['Nickname']) && isset($_POST['Password'])) {
    
    $Nickname = $_POST['Nickname'];
    $Password = $_POST['Password'];

    // mysql inserting a new row
    $result = mysqli_query($conexion, "INSERT INTO `caventure`.`usuarios` (`id`, `nickname`, `clave`, `puntuacion`) VALUES (NULL, '$Nickname', MD5('$Password'), '0');");

    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["message"] = "Exito al crear cuenta.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["message"] = "Error";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["message"] = "Falta una field";

    // echoing JSON response
    echo json_encode($response);
}
?>
