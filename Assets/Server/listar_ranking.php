<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/conexion.php';

// coger todas las puntuaciones
$result = mysqli_query($conexion, "SELECT * FROM `usuarios` ORDER BY `usuarios`.`puntuacion` DESC") or die(mysqli_error());

// check for empty result
if (mysqli_num_rows($result) > 0) {
    // looping through all results
    $response["item"] = array();
    
    while ($row = mysqli_fetch_array($result)) {
        // temp user array
        $puntuacion = array();
        $puntuacion["nickname"] = $row["nickname"];
        $puntuacion["puntuacion"] = $row["puntuacion"];

        // push single product into final response array
        array_push($response["item"], $puntuacion);
    }

    // echoing JSON response
    echo json_encode($response);
} else {
    // no puntuaciones found
    $response["success"] = 0;
    $response["message"] = "No puntuaciones found";

    // echo no users JSON
    echo json_encode($response);
}
?>
