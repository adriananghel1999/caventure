﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionPanel : MonoBehaviour
{
    public GameObject MouseOverGO, objName, objDesc;

    private CanvasGroup canvasgroup;

    private float originalPosX, alternativePosX;


    void Start()
    {
        originalPosX = this.transform.localPosition.x;
        alternativePosX = -(this.transform.localPosition.x);

        canvasgroup = this.gameObject.GetComponent<CanvasGroup>();
        objName = GameObject.Find("Name");
        objDesc = GameObject.Find("Description");
        Hide();
    }

    void Hide()
    {
        canvasgroup.alpha = 0f;
        canvasgroup.blocksRaycasts = false;

    }

    void Show()
    {
        canvasgroup.alpha = 1f;
        canvasgroup.blocksRaycasts = true;
    }

    public void ChangePosition()
    {

        if (this.transform.localPosition.x == originalPosX)
        {
            this.transform.localPosition = new Vector3(alternativePosX, this.transform.localPosition.y, this.transform.localPosition.z);
        }
        else if (this.transform.localPosition.x == alternativePosX)
        {
            this.transform.localPosition = new Vector3(originalPosX, this.transform.localPosition.y, this.transform.localPosition.z);
        }

    }

    void Update()
    {

        if (MouseOverGO)
        {

            if (MouseOverGO.CompareTag("Consumable"))
            {
                Consumable item = (Consumable)MouseOverGO.GetComponent<ItemData>().uniqueItem;
                objName.GetComponent<Text>().text = item.ItemName;
                objDesc.GetComponent<Text>().text = item.Description;
            }
            if (MouseOverGO.CompareTag("Equipable"))
            {
                Equipable item = (Equipable)MouseOverGO.GetComponent<ItemData>().uniqueItem;
                objName.GetComponent<Text>().text = item.ItemName;
                objDesc.GetComponent<Text>().text = item.Description;
            }
            if (MouseOverGO.CompareTag("Hero"))
            {
                Hero hero = (Hero)MouseOverGO.GetComponent<CreatureData>().uniqueCreature;
                objName.GetComponent<Text>().text = hero.creatureName;
                objDesc.GetComponent<Text>().text = hero.showStats();
            }
            if (MouseOverGO.CompareTag("Enemy"))
            {
                Enemy enemy = (Enemy)MouseOverGO.GetComponent<CreatureData>().uniqueCreature;
                objName.GetComponent<Text>().text = enemy.creatureName;
                objDesc.GetComponent<Text>().text = enemy.creatureDescription;
            }
            if (MouseOverGO.CompareTag("Spell"))
            {
                Skill skill = MouseOverGO.GetComponent<SpellData>().uniqueSpell;
                objName.GetComponent<Text>().text = skill.SkillName;
                objDesc.GetComponent<Text>().text = skill.SkillDesc;
            }
            if (MouseOverGO.CompareTag("Achivement"))
            {
                Achivement achivement = MouseOverGO.GetComponent<AchivementData>().achivementData;
                objName.GetComponent<Text>().text = achivement.achivementName;
                objDesc.GetComponent<Text>().text = achivement.achivementDesc;
            }

            Show();

        }
        else
        {
            Hide();
        }
    }
}
