﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
    {

        // Inventario
        public bool[] isFull;
        public GameObject[] slots;
        
        // Hero1

        public bool[] Hero1full;
        public GameObject[] hero1slots;
        public bool[] Hero1fullspells;
        public GameObject[] hero1slotsspells;


        // Hero2

        public bool[] Hero2full;
        public GameObject[] hero2slots;
        public bool[] Hero2fullspells;
        public GameObject[] hero2slotsspells;

        // Hero3

        public bool[] Hero3full;
        public GameObject[] hero3slots;
        public bool[] Hero3fullspells;
        public GameObject[] hero3slotsspells;

        // Hero4

        public bool[] Hero4full;
        public GameObject[] hero4slots;
        public bool[] Hero4fullspells;
        public GameObject[] hero4slotsspells;

    }
