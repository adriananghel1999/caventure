﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemUI : MonoBehaviour
{
    public ScriptableObject ScriptableObjectVar;

    public Transform Inventory;
    public InventoryController inventoryController;

    void Start(){
        Inventory = GameObject.Find("InventarioPanel").transform;
        inventoryController = GameObject.Find("InventarioPanel").GetComponent<InventoryController>();
        

        EventTrigger trigger = GetComponentInParent<EventTrigger>();

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener( (eventData) => { inventoryController.setSelectedItem(this.transform); } );
        trigger.triggers.Add(entry);

        
        
        
    }

    public void destroyItem(){
        Destroy(gameObject);
    }

}
