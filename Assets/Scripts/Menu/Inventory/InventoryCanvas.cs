﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCanvas : MonoBehaviour
{
    private GameObject menu;
    private bool isShowingInventario = false;
    public GameObject InventoryCanvasObject;
    private DescriptionPanel descriptionPanel;

    void Start()
    {   menu = GlobalMenu.Instance.gameObject;
        InventoryCanvasObject = GlobalInventory.Instance.gameObject;
        descriptionPanel = GlobalUI.Instance.gameObject.transform.Find("DescriptionPanel").gameObject.GetComponent<DescriptionPanel>();
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.anyKeyDown && GlobalControl.Instance.batallaActiva == false && GlobalControl.Instance.juegoActivo == true && Input.GetAxisRaw("Inventory") == 1 && menu.activeInHierarchy == false)
        {
            isShowingInventario = !isShowingInventario;
            InventoryCanvasObject.SetActive(isShowingInventario);

            if (descriptionPanel.MouseOverGO != null)
            {
                descriptionPanel.MouseOverGO = null;
            }
        }
    }
}
