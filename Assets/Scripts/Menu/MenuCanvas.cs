﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvas : MonoBehaviour
{
    private bool isShowing = false;
    public GameObject MenuCanvasObject;

    void Start(){
        MenuCanvasObject = GlobalMenu.Instance.gameObject;
    }
    void Update()
    {

        if (Input.anyKeyDown && GlobalControl.Instance.juegoActivo == true && Input.GetAxisRaw("Menu") == 1)
        {
            isShowing = !isShowing;
            MenuCanvasObject.SetActive(isShowing);

        }
    }
}
