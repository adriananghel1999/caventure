﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

    public Animator animator;

    void Start(){
        animator = GameObject.Find("TransitionPanel").GetComponent<Animator>();
    }

    public void LoadByIndex(int sceneIndex)
    {
        StartCoroutine(LoadScene(sceneIndex));
    }

    IEnumerator LoadScene(int sceneIndex){
        
        animator.SetTrigger("start");
        animator.Play("EverythingBlack",-1, 0f);
        yield return new WaitForSeconds(1.5F);
        SceneManager.LoadScene(sceneIndex);
        animator.SetTrigger("end");
    }
}