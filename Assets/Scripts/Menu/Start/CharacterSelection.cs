﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{

    // UI Parte de la derecha

    // Los selecionados
    [SerializeField]
    private List<Hero> heroParty;

    [SerializeField]
    private Button delete;

    // Imagenes current party

    [SerializeField]
    private List<Image> heroesImageArray;

    // UI Parte de la izquierda
    [SerializeField]
    private List<Hero> heroArray;

    [SerializeField]
    private Image HeroImg;
    [SerializeField]
    private Text HeroClassName;
    [SerializeField]
    private Text HeroClassDescription;

    // Botones
    [SerializeField]
    private Button previous;
    [SerializeField]
    private Button next;
    [SerializeField]
    private Button add;
    [SerializeField]
    private Button start;

    // Variables importantes
    private int currentHero = 0;
    private int partyHeroSize = 0;

    // Dificultad
    public Text dificultad;

    // Start is called before the first frame update
    void Start()
    {
        if (GlobalAchivements.Instance.heroUnlocked.Count != 0)
        {
            for (int i = 0; i <= GlobalAchivements.Instance.heroUnlocked.Count - 1; i++)
            {
                heroArray.Add(GlobalAchivements.Instance.heroUnlocked[i]);
            }
        }

        HeroImg.sprite = heroArray[0].Icon;
        HeroClassName.text = heroArray[0].nameCreature;
        HeroClassDescription.text = heroArray[0].descriptionCreature;

        // Listeners
        next.onClick.AddListener(NextHero);
        previous.onClick.AddListener(PreviousHero);
        delete.onClick.AddListener(DeleteHero);
        add.onClick.AddListener(AddHero);

    }

    void Update()
    {
        if (heroParty.Count == 0)
        {
            start.interactable = false;
            dificultad.text = "Difficulty: (Add a member to the party)";
        }
        else
        {
            start.interactable = true;
        }

        if (heroParty.Count == 1){
            dificultad.text = "Difficulty: Easy";
        } else if (heroParty.Count == 2){
            dificultad.text = "Difficulty: Normal";
        } else if (heroParty.Count == 3){
            dificultad.text = "Difficulty: Hard";
        } else if (heroParty.Count == 4){
            dificultad.text = "Difficulty: Very hard";
        }

    }

    void NextHero()
    {
        if (currentHero == heroArray.Count - 1)
        {

        }
        else
        {

            currentHero++;
            HeroImg.sprite = heroArray[currentHero].Icon;
            HeroClassName.text = heroArray[currentHero].nameCreature;
            HeroClassDescription.text = heroArray[currentHero].descriptionCreature;
        }

    }

    void PreviousHero()
    {
        if (currentHero == 0)
        {

        }
        else
        {

            currentHero--;
            HeroImg.sprite = heroArray[currentHero].Icon;
            HeroClassName.text = heroArray[currentHero].nameCreature;
            HeroClassDescription.text = heroArray[currentHero].descriptionCreature;
        }
    }

    void AddHero()
    {
        if (partyHeroSize == 4 || heroArray.Count == 0)
        {

        }
        else
        {
            heroParty.Add(heroArray[currentHero]);
            heroesImageArray[partyHeroSize].sprite = heroArray[currentHero].Icon;
            heroesImageArray[partyHeroSize].color = Color.white;
            heroArray.RemoveAt(currentHero);
            currentHero--;
            partyHeroSize++;

            if (currentHero >= heroArray.Count - 1)
            {
                currentHero = heroArray.Count;
                PreviousHero();
            }
            else
            {
                NextHero();
            }

        }

    }

    void DeleteHero()
    {
        if (partyHeroSize == 0)
        {

        }
        else
        {
            heroArray.Add(heroParty[partyHeroSize - 1]);
            heroParty.RemoveAt(partyHeroSize - 1);
            heroesImageArray[partyHeroSize - 1].color = Color.black;
            partyHeroSize--;
        }

    }

    public void StartGame()
    {

        for (int i = 0; i < heroParty.Count; i++)
        {
            Hero hero = Object.Instantiate(heroParty[i]);
            GlobalControl.Instance.heroParty.Add(hero);
        }

        if (GlobalControl.Instance.heroParty.Count == 1)
        {
            GlobalControl.Instance.dificultad = 0;
        }
        else if (GlobalControl.Instance.heroParty.Count == 2)
        {
            GlobalControl.Instance.dificultad = 1;
        }
        else if (GlobalControl.Instance.heroParty.Count == 3)
        {
            GlobalControl.Instance.dificultad = 2;
        }
        else if (GlobalControl.Instance.heroParty.Count == 4)
        {
            GlobalControl.Instance.dificultad = 3;
        }

        Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.startGame;
        Music.Instance.gameObject.GetComponent<AudioSource>().Stop();
        Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
    }
}
