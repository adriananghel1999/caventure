﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectWhoShowsInPanel : MonoBehaviour
{
    public DescriptionPanel ObjectInfoUI;


    // Start is called before the first frame update
    void Start()
    {

        if (GameObject.Find("DescriptionPanel").GetComponent<DescriptionPanel>())
        {
            ObjectInfoUI = GameObject.Find("DescriptionPanel").GetComponent<DescriptionPanel>();
        }

        EventTrigger trigger = GetComponentInParent<EventTrigger>();

        EventTrigger.Entry entryInfoUI = new EventTrigger.Entry();
        entryInfoUI.eventID = EventTriggerType.PointerEnter;
        entryInfoUI.callback.AddListener((eventData) => { ObjectInfoUI.MouseOverGO = this.gameObject; });
        trigger.triggers.Add(entryInfoUI);

        EventTrigger.Entry entryInfoUIout = new EventTrigger.Entry();
        entryInfoUIout.eventID = EventTriggerType.PointerExit;
        entryInfoUIout.callback.AddListener((eventData) => { ObjectInfoUI.MouseOverGO = null; });
        trigger.triggers.Add(entryInfoUIout);

    }
}
