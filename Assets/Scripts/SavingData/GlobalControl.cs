﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalControl : MonoBehaviour
{
    public static GlobalControl Instance;

    // Nivel mazmorra

    public int nivel;

    // Dificultad 0 facil, 1 normal, 2 dificil, 3 muy dificil

    public int dificultad;

    // Puntuacion

    public int puntuacion;

    // Party
    public List<Hero> heroParty;

    // Escena NewGame
    GameObject game;
    bool gameNoEstaNull = false;

    // Si esta ocurriendo una batalla

    public bool batallaActiva = false;

    // Si la batalla es con un boss

    public bool batallaBoss = false;

    // Si la batalla es con un elite

    public bool batallaElite = false;

    // Si la partida esta activa
    public bool juegoActivo = false;

    void Awake()
    {

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

    }

    void Update()
    {
        if (game == null && SceneManager.GetActiveScene().name == "NewGame")
        {
            game = GameObject.Find("NewGame");
            gameNoEstaNull = true;
        }
        if (batallaActiva == false && gameNoEstaNull == true)
        {
            game.SetActive(true);
        }
    }

    public void clearGame()
    {
        game = null;
        gameNoEstaNull = false;
        nivel = 1;
        puntuacion = 0;
        juegoActivo = false;
        heroParty.Clear();
    }

}

