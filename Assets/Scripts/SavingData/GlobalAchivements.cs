﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GlobalAchivements : MonoBehaviour
{

    public static GlobalAchivements Instance;

    public List<Hero> heroUnlocked;
    public List<Hero> heroLocked;
    public Achivement[] achivementsScriptableObject;

    public int bossKilled = 0;
    public int eliteKilled = 0;


    public void saveProgress()
    {

        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/progress.data";
        FileStream stream = new FileStream(path, FileMode.Create);

        AchivementsData data = new AchivementsData();

        for (int i = 0; i < achivementsScriptableObject.Length; i++)
        {
            if (achivementsScriptableObject[i].itIsDone == true)
            {
                data.achivementsCompleted[i] = true;
            }
        }

        data.bossKilled = bossKilled;
        data.eliteKilled = eliteKilled;

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public void loadProgress()
    {
        string path = Application.persistentDataPath + "/progress.data";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            AchivementsData data = formatter.Deserialize(stream) as AchivementsData;
            stream.Close();

            for (int i = 0; i < data.achivementsCompleted.Length; i++)
            {
                if (data.achivementsCompleted[i] == true)
                {
                    achivementsScriptableObject[i].itIsDone = true;
                }
            }

            bossKilled = data.bossKilled;
            eliteKilled = data.eliteKilled;
        }
        else
        {
            Debug.Log("Save file not found in " + path);
        }
    }

    void Start()
    {
        loadProgress();

        for (int i = 0; i < achivementsScriptableObject.Length; i++)
        {
            if (achivementsScriptableObject[i].itIsDone == true)
            {
                heroUnlocked.Add(heroLocked[i]);
            }
        }
    }

    void Awake()
    {

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

    }

}
