﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Audio;

public class GlobalOptions : MonoBehaviour
{
    public static GlobalOptions Instance;

    public AudioSource music;
    public AudioSource sound;
    public float musicVolume = 0.5f;
    public float soundVolume = 0.5f;
    public bool shareScore = false;
    public bool rememberSession = false;

    [SerializeField]
    private string nicknameActualSession;
    [SerializeField]
    private string passwordActualSession;

    void OnEnable()
    {
        loadPreferences();

        if (rememberSession == true)
        {
            if (loadSession() == null)
            {

            }
            else
            {
                nicknameActualSession = loadSession().sessionNickname;
                passwordActualSession = loadSession().sessionPassword;
            }

        }

    }

    void Update(){
        music.volume = musicVolume;
        sound.volume = soundVolume;
    }

    void Awake()
    {

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

    }

    public void saveSession()
    {
        if (nicknameActualSession != null && passwordActualSession != null)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/session.data";
            FileStream stream = new FileStream(path, FileMode.Create);

            SessionData data = new SessionData();

            data.sessionNickname = nicknameActualSession;
            data.sessionPassword = passwordActualSession;

            formatter.Serialize(stream, data);
            stream.Close();
        }
    }

    public SessionData loadSession()
    {
        string path = Application.persistentDataPath + "/session.data";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SessionData data = formatter.Deserialize(stream) as SessionData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.Log("Save file not found in " + path);
            return null;
        }
    }
    [System.Serializable]
    public class SessionData
    {
        public string sessionNickname;
        public string sessionPassword;
    }

    public void setMusicVolume(GameObject value)
    {
        musicVolume = value.GetComponent<Slider>().value;
    }

    public void setSoundVolume(GameObject value)
    {
        soundVolume = value.GetComponent<Slider>().value;
    }

    public void setScorePreference(GameObject value)
    {
        shareScore = value.GetComponent<Toggle>().isOn;
    }

    public void setSessionPreference(GameObject value)
    {
        rememberSession = value.GetComponent<Toggle>().isOn;
    }

    public void savePreferences()
    {
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
        PlayerPrefs.SetFloat("SoundVolume", soundVolume);

        if (shareScore == true)
        {
            PlayerPrefs.SetInt("ShareScore", 1);
        }
        else
        {
            PlayerPrefs.SetInt("ShareScore", 0);
        }

        if (rememberSession == true)
        {
            PlayerPrefs.SetInt("RememberSession", 1);
            saveSession();
        }
        else
        {
            PlayerPrefs.SetInt("RememberSession", 0);
            setNicknameActualSession("");
            setPasswordActualSession("");
            saveSession();
        }

        PlayerPrefs.Save();

    }

    public void loadPreferences()
    {
        musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        soundVolume = PlayerPrefs.GetFloat("SoundVolume");

        if (PlayerPrefs.GetInt("ShareScore") == 1)
        {
            shareScore = true;
        }
        else
        {
            shareScore = false;
        }

        if (PlayerPrefs.GetInt("RememberSession") == 1)
        {
            rememberSession = true;
        }
        else
        {
            rememberSession = false;
        }

    }

    public void setNicknameActualSession(string value)
    {
        this.nicknameActualSession = value;
    }

    public void setPasswordActualSession(string value)
    {
        this.passwordActualSession = value;
    }

    public string getNicknameActualSession()
    {
        return this.nicknameActualSession;
    }

    public string getPasswordActualSession()
    {
        return this.passwordActualSession;
    }

}
