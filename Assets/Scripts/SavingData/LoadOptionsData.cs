﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadOptionsData : MonoBehaviour
{

    public GameObject music;
    public GameObject sound;
    public GameObject scoreShare;
    public GameObject rememberSession;
    public GameObject BackBtn;

    void OnEnable()
    {
        music.GetComponent<Slider>().value = GlobalOptions.Instance.musicVolume;
        sound.GetComponent<Slider>().value = GlobalOptions.Instance.soundVolume;
        scoreShare.GetComponent<Toggle>().isOn = GlobalOptions.Instance.shareScore;
        rememberSession.GetComponent<Toggle>().isOn = GlobalOptions.Instance.rememberSession;

        music.GetComponent<Slider>().onValueChanged.AddListener(delegate { GlobalOptions.Instance.setMusicVolume(music.gameObject); });
        sound.GetComponent<Slider>().onValueChanged.AddListener(delegate { GlobalOptions.Instance.setSoundVolume(sound.gameObject); });
        scoreShare.GetComponent<Toggle>().onValueChanged.AddListener(delegate { GlobalOptions.Instance.setScorePreference(scoreShare.gameObject); });
        rememberSession.GetComponent<Toggle>().onValueChanged.AddListener(delegate { GlobalOptions.Instance.setSessionPreference(rememberSession.gameObject); });
        BackBtn.GetComponent<Button>().onClick.AddListener(delegate { GlobalOptions.Instance.savePreferences(); });

    }
}
