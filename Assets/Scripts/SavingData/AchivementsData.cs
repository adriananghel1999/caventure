﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


[System.Serializable]
public class AchivementsData
{

    [SerializeField]
    public bool[] achivementsCompleted = new bool[]{false,false,false,false,false};

    [SerializeField]
    public int bossKilled = 0;
    [SerializeField]
    public int eliteKilled = 0;


}
