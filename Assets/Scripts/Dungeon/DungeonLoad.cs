﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class DungeonLoad : MonoBehaviour
{
    // Antorchas
    public List<GameObject> antorchas;

    // Inventario
    GameObject hero1img, hero2img, hero3img, hero4img, hero1imgspells, hero2imgspells, hero3imgspells, hero4imgspells, canvasInventario, canvasMenu, hero1, hero2, hero3, hero4, hero1spells, hero2spells, hero3spells, hero4spells, SpellsPanel;
    List<GameObject> HeroesUI, HeroesSpells;
    List<GameObject> HeroesObjects, HeroesObjectsSpells;

    // Start is called before the first frame update
    void Start()
    {
        GlobalControl.Instance.juegoActivo = true;

        canvasInventario = GlobalInventory.Instance.gameObject;
        canvasMenu = GlobalMenu.Instance.gameObject;
        SpellsPanel = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells").gameObject;

        HeroesUI = new List<GameObject>();
        HeroesSpells = new List<GameObject>();
        HeroesObjects = new List<GameObject>();
        HeroesObjectsSpells = new List<GameObject>();

        try
        {
            hero1img = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero1/hero1img").gameObject;
            hero1imgspells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero1spells/hero1imgspells").gameObject;
            hero1 = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero1").gameObject;
            hero1spells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero1spells").gameObject;
        }
        catch (Exception e) { }

        try
        {
            hero2img = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero2/hero2img").gameObject;
            hero2imgspells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero2spells/hero2imgspells").gameObject;
            hero2 = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero2").gameObject;
            hero2spells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero2spells").gameObject;
        }
        catch (Exception e) { }

        try
        {
            hero3img = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero3/hero3img").gameObject;
            hero3imgspells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero3spells/hero3imgspells").gameObject;
            hero3 = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero3").gameObject;
            hero3spells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero3spells").gameObject;
        }
        catch (Exception e) { }

        try
        {
            hero4img = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero4/hero4img").gameObject;
            hero4imgspells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero4spells/hero4imgspells").gameObject;
            hero4 = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment/hero4").gameObject;
            hero4spells = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells/hero4spells").gameObject;
        }
        catch (Exception e) { }

        HeroesUI.Add(hero1img);
        HeroesUI.Add(hero2img);
        HeroesUI.Add(hero3img);
        HeroesUI.Add(hero4img);
        HeroesObjects.Add(hero1);
        HeroesObjects.Add(hero2);
        HeroesObjects.Add(hero3);
        HeroesObjects.Add(hero4);

        HeroesSpells.Add(hero1imgspells);
        HeroesSpells.Add(hero2imgspells);
        HeroesSpells.Add(hero3imgspells);
        HeroesSpells.Add(hero4imgspells);
        HeroesObjectsSpells.Add(hero1spells);
        HeroesObjectsSpells.Add(hero2spells);
        HeroesObjectsSpells.Add(hero3spells);
        HeroesObjectsSpells.Add(hero4spells);

        for (int i = 0; i < GlobalControl.Instance.heroParty.Count; i++)
        {
            HeroesSpells[i].GetComponent<Image>().sprite = GlobalControl.Instance.heroParty[i].Icon;
            HeroesSpells[i].GetComponent<CreatureData>().uniqueCreature = GlobalControl.Instance.heroParty[i];

            HeroesUI[i].GetComponent<Image>().sprite = GlobalControl.Instance.heroParty[i].Icon;
            HeroesUI[i].GetComponent<CreatureData>().uniqueCreature = GlobalControl.Instance.heroParty[i];


        }

        for (int i = 0; i < HeroesUI.Count; i++)
        {
            try
            {
                if (HeroesUI[i].GetComponent<Image>().sprite == null || (!(HeroesUI[i].gameObject)))
                {
                    Destroy(HeroesObjects[i]);
                    Destroy(HeroesObjectsSpells[i]);
                }
            }
            catch (Exception e)
            {

            }
        }
        canvasInventario.GetComponent<CanvasGroup>().alpha = 0;
        canvasInventario.GetComponent<CanvasGroup>().blocksRaycasts = false;
        canvasMenu.GetComponent<CanvasGroup>().alpha = 0;
        canvasMenu.GetComponent<CanvasGroup>().blocksRaycasts = false;
        StartCoroutine(waitForLoad(1f));
    }

    public void todasLasAntorchasEncendidas()
    {
        bool antorchasEncendidas = false;
        for (int i = 0; i < antorchas.Count; i++)
        {
            if (antorchas[i].GetComponent<DungeonInteraction>().luzEncendida == false)
            {
                antorchasEncendidas = false;
                break;
            }
            else
            {
                antorchasEncendidas = true;
            }

        }
        if (antorchasEncendidas == true)
        {
            GameObject[] murosFantasma = GameObject.FindGameObjectsWithTag("MuroFantasma");
            for (var i = 0; i < murosFantasma.Length; i++)
            {
                Destroy(murosFantasma[i]);
            }

        }
    }

    IEnumerator waitForLoad(float time)
    {

        yield return new WaitForSeconds(time);
        Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.ambient[Music.Instance.randomNumber()];
        Music.Instance.gameObject.GetComponent<AudioSource>().Play();
        canvasInventario.SetActive(false);
        canvasMenu.SetActive(false);
        SpellsPanel.SetActive(false);
        canvasInventario.GetComponent<CanvasGroup>().alpha = 1;
        canvasInventario.GetComponent<CanvasGroup>().blocksRaycasts = true;
        canvasMenu.GetComponent<CanvasGroup>().alpha = 1;
        canvasMenu.GetComponent<CanvasGroup>().blocksRaycasts = true;
        antorchas.RemoveAt(antorchas.Count - 1);

    }
}
