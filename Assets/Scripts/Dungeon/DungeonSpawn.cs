﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonSpawn : MonoBehaviour
{
    public int openingDirection;
    // 1 --> necesita puerta abajo
    // 2 --> necesita puerta arriba
    // 3 --> necesita puerta izquierda
    // 4 --> necesita puerta derecha

    private RoomTemplate templates;
    private int rand;
    private bool spawned = false;

    public float waitTime = 4f;

    void Start()
    {
        templates = GameObject.Find("RoomTemplate").GetComponent<RoomTemplate>();
        Invoke("Spawn", 0.1f);
        Destroy(gameObject, waitTime);
        
    }

    void Spawn()
    {
        if (spawned == false)
        {
            if (openingDirection == 1)
            {
                rand = Random.Range(0, templates.bottomRooms.Length);
                Instantiate(templates.bottomRooms[rand], transform.position, templates.bottomRooms[rand].transform.rotation, GameObject.Find("NewGame").transform);
            }
            else if (openingDirection == 2)
            {
                rand = Random.Range(0, templates.topRooms.Length);
                Instantiate(templates.topRooms[rand], transform.position, templates.topRooms[rand].transform.rotation, GameObject.Find("NewGame").transform);
            }
            else if (openingDirection == 3)
            {
                rand = Random.Range(0, templates.leftRooms.Length);
                Instantiate(templates.leftRooms[rand], transform.position, templates.leftRooms[rand].transform.rotation, GameObject.Find("NewGame").transform);
            }
            else if (openingDirection == 4)
            {
                rand = Random.Range(0, templates.rightRooms.Length);
                Instantiate(templates.rightRooms[rand], transform.position, templates.rightRooms[rand].transform.rotation, GameObject.Find("NewGame").transform);
            }
            spawned = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("DungeonSpawn"))
        {

            if (other.GetComponent<DungeonSpawn>().spawned == false && spawned == false)
            {
                Instantiate(templates.closedRoom, transform.position, Quaternion.identity, GameObject.Find("NewGame").transform);
                Destroy(gameObject);
            }
            spawned = true;

        }
    }
}
