﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnJugador : MonoBehaviour
{
    public GameObject[] objects;
    public GameObject objectoUnico;
    void Start()
    {
        int rand = Random.Range(0, objects.Length);
        Vector2 coord = objects[rand].GetComponent<Transform>().position;
        Instantiate(objectoUnico, coord, Quaternion.identity, GameObject.Find("NewGame").transform);

        
    }
}
