﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
//Sirve para destruir algunos objetos que spawnean cosas
    void Start(){
        Destroy(gameObject, 0.5f);
    }

    void OnTriggerEnter2D(Collider2D other){
        Destroy(other.gameObject);
    }
}
