﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IA : MonoBehaviour
{
    // Para transicion
    public Animator animator;
    // Para otra cosa
    GameObject newgame;
    GameObject jugador;
    AsyncOperation asyncLoadLevel;

    void Start()
    {
        newgame = GameObject.Find("NewGame");
        jugador = GameObject.Find("Jugador");
        animator = GameObject.Find("TransitionPanel").GetComponent<Animator>();

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.enemyTouch;
            Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
            if (gameObject.name == "Boss")
            {
                GlobalControl.Instance.batallaBoss = true;
            }

            if (gameObject.name == "Elite(Clone)")
            {
                GlobalControl.Instance.batallaElite = true;
            }

            StartCoroutine(AsynchronousLoad());


        }

    }

    IEnumerator AsynchronousLoad()
    {
        animator.Play("EverythingWhite",-1, 0f);
        jugador.GetComponent<Transform>().position = gameObject.GetComponent<Transform>().position;
        GlobalControl.Instance.batallaActiva = true;
        yield return new WaitForSeconds(1F);
        SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive);
        Destroy(gameObject);
        newgame.SetActive(false);
        
        
        
        
        
    }
}
