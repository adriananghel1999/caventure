﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonInteraction : MonoBehaviour
{

    // Instancia del inventario para poder relacionarse con el cofre

    public Inventory inventory;

    private void Start()
    {
        inventory = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel").gameObject.GetComponent<Inventory>();

        if (gameObject.tag == "Antorcha")
        {
            GameObject.Find("NewGame").GetComponent<DungeonLoad>().antorchas.Add(this.gameObject);
        }
    }

    // Luz antorcha
    public GameObject luzantorcha;
    public bool luzEncendida = false;

    // Abrir cofre
    [SerializeField]
    Sprite openChestSprite;
    private bool cofreAbierto = false;
    public GameObject[] objetosPosibles;

    // Pulsar boton
    [SerializeField]
    Sprite pushButtonSprite;
    private bool botonPulsado = false;

    // Encender antorcha
    Animator anim;

    // Funciones cofre
    public void OpenChest()
    {

        if (cofreAbierto == false)
        {
            Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.chestOpen;
            Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
            gameObject.GetComponent<SpriteRenderer>().sprite = openChestSprite;
            cofreAbierto = true;
            int rand = Random.Range(0, objetosPosibles.Length);

            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (inventory.isFull[i] == false)
                {
                    inventory.isFull[i] = true;
                    Instantiate(objetosPosibles[rand], inventory.slots[i].transform, false);
                    break;
                }
            }
        }


    }

    // Funciones boton
    public void PushButton()
    {

        if (botonPulsado == false)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = pushButtonSprite;
            botonPulsado = true;
        }


    }

    // Funciones antorcha
    public void LightTorch()
    {

        if (luzEncendida == false)
        {
            Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.torchUp;
            Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
            anim = GetComponent<Animator>();
            anim.Play("TorchLighted");
            luzantorcha.SetActive(true);
            luzEncendida = true;
        }

        GameObject.Find("NewGame").GetComponent<DungeonLoad>().todasLasAntorchasEncendidas();

    }



}
