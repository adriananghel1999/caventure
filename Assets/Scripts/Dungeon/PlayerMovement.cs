﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Inventario, esto sirve por si el inventario esta activo, el personaje que no se pueda mover

    private GameObject inventario;
    private GameObject menu;

    // Interaccion
    public GameObject currentInterObj = null;

    // Movimiento
    Vector3 pos;                                // For movement
    Vector3 oldpos;
    float speed = 3.0f;                         // Speed of movement

    void Start()
    {
        // Movimiento
        pos = transform.position;          // Take the initial position

        // Get inventario

        inventario = GlobalInventory.Instance.gameObject;

        // Get menu

        menu = GlobalMenu.Instance.gameObject;


    }

    void FixedUpdate()
    {

        // Movimiento
        oldpos = pos;
        if (Input.GetAxisRaw("Horizontal") == -1 && transform.position == pos && inventario.activeInHierarchy == false && menu.activeInHierarchy == false && GlobalControl.Instance.batallaActiva == false)
        {        // Left
            
            pos += Vector3.left;
        }
        else if (Input.GetAxisRaw("Horizontal") == 1 && transform.position == pos && inventario.activeInHierarchy == false && menu.activeInHierarchy == false && GlobalControl.Instance.batallaActiva == false)
        {        // Right
            
            pos += Vector3.right;
        }
        else if (Input.GetAxisRaw("Vertical") == 1 && transform.position == pos && inventario.activeInHierarchy == false && menu.activeInHierarchy == false && GlobalControl.Instance.batallaActiva == false)
        {        // Up
            
            pos += Vector3.up;
        }
        else if (Input.GetAxisRaw("Vertical") == -1 && transform.position == pos && inventario.activeInHierarchy == false && menu.activeInHierarchy == false && GlobalControl.Instance.batallaActiva == false)
        {        // Down
            
            pos += Vector3.down;
        }
        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);    // Move there

        //Interaccion enviar mensaje
        if (Input.GetAxisRaw("Interaction") == 1 && currentInterObj){

            if (currentInterObj.tag == "Cofre"){
                
                currentInterObj.SendMessage("OpenChest");
                
            }

            if (currentInterObj.tag == "Boton"){
                
                currentInterObj.SendMessage("PushButton");
                
            }

            if (currentInterObj.tag == "Antorcha"){
                
                currentInterObj.SendMessage("LightTorch");
                
            }

        }
        
    }

// Colisiones
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Muro" || collision.gameObject.tag == "MuroFantasma")
        {
            pos = oldpos;

        }

        
    }

// Interacciones al tocar objeto

    void OnTriggerEnter2D(Collider2D other){
        currentInterObj = other.gameObject;

        if (other.gameObject.tag == "DungeonSpawn")
      {
          Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other);
      }

    }

// Interacciones al quedarse en el area del objeto
void OnTriggerStay2D(Collider2D other){

        if (other.CompareTag("Cofre") && Input.GetKey(KeyCode.E))
        {
            

        }

        if (other.CompareTag("Boton") && Input.GetKey(KeyCode.E))
        {
            

        }

        if (other.CompareTag("Antorcha") && Input.GetKey(KeyCode.E))
        {
            //m_OrthographicCamera.orthographicSize = m_OrthographicCamera.orthographicSize + 1;
            //other.gameObject.tag = "Untagged";
        }
}

// Interacciones al salir del area del objeto

    void OnTriggerExit2D(Collider2D other){
        if (other.gameObject == currentInterObj){
            currentInterObj = null;
        }
    }
}
