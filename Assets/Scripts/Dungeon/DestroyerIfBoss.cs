﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerIfBoss : MonoBehaviour
{
    public GameObject test;

    void OnEnable()
    {
        if (test = GameObject.FindGameObjectWithTag("Player"))
        {
            Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        test = collision.gameObject;

        if (collision.gameObject.name == "Boss")
        {
            Destroy(gameObject);
        }

    }

}
