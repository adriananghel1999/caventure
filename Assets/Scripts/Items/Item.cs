using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject
{
    [SerializeField]
    private string itemName;
    [SerializeField]
    private string description;

    [SerializeField]
    private int modifierST;
    [SerializeField]
    private int modifierDE;
    [SerializeField]
    private int modifierPE;
    [SerializeField]
    private int modifierINTE;
    [SerializeField]
    private int modifierHP;
    [SerializeField]
    private int currentHP;

    public int ModifierST{
        get{
            return modifierST;
        }
        set{
            modifierST = value;
        }
    }

    public int ModifierDE{
        get{
            return modifierDE;
        }
        set{
            modifierDE = value;
        }
    }

    public int ModifierPE{
        get{
            return modifierPE;
        }
        set{
            modifierPE = value;
        }
    }

    public int ModifierINTE{
        get{
            return modifierINTE;
        }
        set{
            modifierINTE = value;
        }
    }

    public int ModifierHP{
        get{
            return modifierHP;
        }
        set{
            modifierHP = value;
        }
    }

    public int CurrentHP{
        get{
            return currentHP;
        }
        set{
            currentHP = value;
        }
    }

    public string ItemName{
        get{
            return itemName;
        }

        set{
            itemName = value;
        }
    }

    public string Description{
        get{
            return description;
        }

        set{
            description = value;
        }
    }

}
