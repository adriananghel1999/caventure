﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy", order = 51)]
public class Enemy : Creature
{

    [SerializeField]
    private GameObject[] loot;
    public enum enemyType { Normal, Elite, Boss };
    [SerializeField]
    private enemyType type;

    public GameObject[] Loot
    {
        get
        {
            return loot;

        }

        set
        {
            loot = value;
        }
    }

    public enemyType Type
    {
        get
        {
            return type;

        }

        set
        {
            Type = value;
        }
    }

    public override int HP
    {
        get
        {
            if (GlobalControl.Instance.dificultad == 0)
            {
                return (int)((hpBase/2)+(hpBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 1)
            {
                return (int)(hpBase+(hpBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 2)
            {
                return (int)((hpBase*1.25f)+(hpBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 3)
            {
                return (int)((hpBase*1.5f)+(hpBase*calcularMejoraNivel()));
            }

            return hpBase;

        }

        set
        {
            hpBase = value;
        }
    }

    public override int ST
    {
        get
        {
            if (GlobalControl.Instance.dificultad == 0)
            {
                return (int)((stBase/2)+(stBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 1)
            {
                return (int)(stBase+(stBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 2)
            {
                return (int)((stBase*1.25f)+(stBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 3)
            {
                return (int)((stBase*1.3f)+(stBase*calcularMejoraNivel()));
            }

            return stBase;

        }

        set
        {
            stBase = value;
        }
    }

    public override int DE
    {
        get
        {
            if (GlobalControl.Instance.dificultad == 0)
            {
                return (int)((deBase/2)+(deBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 1)
            {
                return (int)(deBase+(deBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 2)
            {
                return (int)((deBase*1.25f)+(deBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 3)
            {
                return (int)((deBase*1.25f)+(deBase*calcularMejoraNivel()));
            }

            return deBase;

        }

        set
        {
            deBase = value;
        }
    }

    public override int PE
    {
        get
        {
            if (GlobalControl.Instance.dificultad == 0)
            {
                return (int)((peBase/2)+(peBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 1)
            {
                return (int)(peBase+(peBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 2)
            {
                return (int)((peBase*1.25f)+(peBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 3)
            {
                return (int)((peBase*1.3f)+(peBase*calcularMejoraNivel()));
            }

            return peBase;

        }

        set
        {
            peBase = value;
        }
    }

    public override int INTE
    {
        get
        {
            if (GlobalControl.Instance.dificultad == 0)
            {
                return (int)((inteBase/2)+(inteBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 1)
            {
                return (int)(inteBase+(inteBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 2)
            {
                return (int)((inteBase*1.25f)+(inteBase*calcularMejoraNivel()));
            }
            else if (GlobalControl.Instance.dificultad == 3)
            {
                return (int)((inteBase*1.3f)+(inteBase*calcularMejoraNivel()));
            }

            return inteBase;

        }

        set
        {
            inteBase = value;
        }
    }

    override public int damage{
        get{
            int damage = 0;
            
            if(attacktype == attackType.Mele){
                damage = ST;
            } else if (attacktype == attackType.Distance){
                damage = PE;
            } else if (attacktype == attackType.Hybrid){
                damage = ST + PE;
            }

            return damage;
        }
    }

    private float calcularMejoraNivel(){
        float modificador = (GlobalControl.Instance.nivel * 0.25f);
        return modificador;
    }

}
