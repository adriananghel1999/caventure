﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Creature : ScriptableObject
{
    [SerializeField]
    public int stBase; //strength
    [SerializeField]
    public int hpBase; //health points
    [SerializeField]
    public int deBase; //dextery
    [SerializeField]
    public int peBase; //perception
    [SerializeField]
    public int inteBase; //inteligence
    [SerializeField]
    public int currentHp; // vida actual

    [SerializeField]
    public List<Skill> skills;

    [SerializeField]
    public string nameCreature;
    [SerializeField]
    public string descriptionCreature;

    public enum attackType{Mele,Distance,Hybrid};
    public enum status { Alive, Defend, Dead };

    [SerializeField]
    public attackType attacktype;
    [SerializeField]
    public status Status = status.Alive;
    

    virtual public int HP{
        get{
            return hpBase + (stBase * 2);
        }

        set {
            hpBase = value;
        }
    }

    virtual public int CurrentHP{
        get{
            return currentHp;
        }

        set {
            currentHp = value;
        }
    }

    virtual public int ST{
        get{
            return stBase;
        }

        set {
            stBase = value;
        }
    }

    virtual public int DE{
        get{
            return deBase;
        }

        set {
            deBase = value;
        }
    }

    virtual public int PE{
        get{
            return peBase;
        }

        set {
            peBase = value;
        }
    }

    virtual public int INTE{
        get{
            return inteBase;
        }

        set {
            inteBase = value;
        }
    }

    virtual public string creatureName{
        get{
            return nameCreature;
        }

        set {
            nameCreature = value;
        }
    }

    virtual public string creatureDescription{
        get{
            return descriptionCreature;
        }

        set {
            descriptionCreature = value;
        }
    }

    virtual public List<Skill> Skills{
        get{
            return skills;
        }
        set{
            skills = value;
        }
    }

    virtual public int damage{
        get{
            int damage = 0;
            
            if(attacktype == attackType.Mele){
                damage = ST;
            } else if (attacktype == attackType.Distance){
                damage = PE;
            } else if (attacktype == attackType.Hybrid){
                damage = ST + PE;
            }

            return damage;
        }
    }
    
}
