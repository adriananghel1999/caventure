﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Hero", menuName = "Hero", order = 51)]
[System.Serializable]
public class Hero : Creature
{
    [SerializeField]
    public Item[] equipedItems;
    [SerializeField]
    private Sprite icon;

    public override int HP{
        get{
                int modifier = 0;

                for (int i = 0; i < equipedItems.Length; i++){
                    if(equipedItems[i] != null){
                        modifier += equipedItems[i].ModifierHP;
                    }
                }

                return hpBase + modifier + ST;
            
        }

        set {
            hpBase = value;
        }
    }

    public override int ST{
        get{
                int modifier = 0;

                for (int i = 0; i < equipedItems.Length; i++){
                    if(equipedItems[i] != null){
                        modifier += equipedItems[i].ModifierST;
                    }
                }
                
                return stBase + modifier;
            
        }

        set {
            stBase = value;
        }
    }

    public override int DE{
        get{
                int modifier = 0;

                for (int i = 0; i < equipedItems.Length; i++){
                    if(equipedItems[i] != null){
                        modifier += equipedItems[i].ModifierDE;
                    }
                }
                
                return deBase + modifier;
            
        }

        set {
            deBase = value;
        }
    }

    public override int PE{
        get{
                int modifier = 0;

                for (int i = 0; i < equipedItems.Length; i++){
                    if(equipedItems[i] != null){
                        modifier += equipedItems[i].ModifierPE;
                    }
                }
                
                return peBase + modifier;
            
        }

        set {
            peBase = value;
        }
    }

    public override int INTE{
        get{
                int modifier = 0;

                for (int i = 0; i < equipedItems.Length; i++){
                    if(equipedItems[i] != null){
                        modifier += equipedItems[i].ModifierINTE;
                    }
                }
                
                return inteBase + modifier;
            
        }

        set {
            inteBase = value;
        }
    }

    public Sprite Icon{
        get{
            return icon;
        }
        set {
            icon = value;
        }
    }

    public string showStats(){
        return "HP: " + currentHp + "/" + this.HP + "\n" + "ST: " + this.ST + "\n" + "DE: " + this.DE + "\n" + "PE: " + this.PE + "\n" + "INTE: " + this.INTE + "\n" + "Attack type: " + this.attacktype;
    }

    public void eatConsumable(Consumable consumable){
        this.hpBase += consumable.ModifierHP;
        this.deBase += consumable.ModifierDE;
        this.inteBase += consumable.ModifierINTE;
        this.peBase += consumable.ModifierPE;
        this.stBase += consumable.ModifierST;
        this.CurrentHP += consumable.CurrentHP;

        if(CurrentHP > this.HP){
            CurrentHP = this.HP;
        }
    }

    public void learnSkill(Skill skill){
            skills.Add(skill);

    }

    public void forgetSkill(Skill skill){
        skills.Remove(skill);
    }

    public Item[] EquipedItems{
        get{
            return equipedItems;
        }
        set{
            equipedItems = value;
        }
    }

    public void setItem(int slot, Item item){
        equipedItems[slot] = item;
    }

    public void setSpell(int slot, Skill skill){
        Skills[slot] = skill;
    }

    override public int damage{
        get{
            int damage = 0;

            if(attacktype == attackType.Mele){
                damage = this.ST;
            } else if (attacktype == attackType.Distance){
                damage = this.PE;
            } else if (attacktype == attackType.Hybrid){
                damage = this.ST + this.PE;
            }

            return damage;
        }
    }

}
