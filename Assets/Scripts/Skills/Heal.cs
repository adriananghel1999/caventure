using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Heal Skill", menuName = "Heal skill", order = 51)]
public class Heal : Skill
{
    [SerializeField]
    private float reduction = 0;
    [SerializeField]
    private float multiplier = 0;

    public float Reduction{
        get{
            return reduction;
        }
        set {
            reduction = value;
        }
    }

    public float Multiplier{
        get{
            return multiplier;
        }
        set {
            multiplier = value;
        }
    }

    public int Healing(Hero hero){
        float heal = hero.INTE;

        if(Reduction != 0){
            heal = heal/Reduction;
        }

        if(Multiplier != 0){
            heal = heal*Multiplier;
        }

        return (int)heal;
    }


}
