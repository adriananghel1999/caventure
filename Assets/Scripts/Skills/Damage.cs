using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Damage Skill", menuName = "Damage skill", order = 51)]
public class Damage : Skill
{

    public enum attackType{Mele,Distance,Hybrid,Inteligence};
    [SerializeField]
    private attackType AttackType;
    [SerializeField]
    private float reduction = 0;
    [SerializeField]
    private float multiplier = 0;

    public attackType DamageType{
        get{
            return AttackType;
        }
        set {
            AttackType = value;
        }
    }

    public float Reduction{
        get{
            return reduction;
        }
        set {
            reduction = value;
        }
    }

    public float Multiplier{
        get{
            return multiplier;
        }
        set {
            multiplier = value;
        }
    }

    public int damage(Hero hero){
        float damage = 0;

        if(DamageType == attackType.Distance){
            damage = hero.PE;
        } else if(DamageType == attackType.Mele){
            damage = hero.ST;
        } else if(DamageType == attackType.Hybrid){
            damage = hero.ST + hero.PE;
        } else if(DamageType == attackType.Inteligence){
            damage = hero.INTE;
        }

        if(Reduction != 0){
            damage = damage/Reduction;
        }

        if(Multiplier != 0){
            damage = damage*Multiplier;
        }

        return (int)damage;
    }


}
