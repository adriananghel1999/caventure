using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : ScriptableObject
{

    [SerializeField]
    public int coulddown; // Enfriamiento
    [SerializeField]
    public string skillName; // Nombre
    [SerializeField]
    public string skillDesc; // Descripcion
    [SerializeField]
    public int targets; // Numero objetivos

    public int coulddownFinish = 0;

    virtual public int Coulddown{
        get{
            return coulddown;
        }
        set {
            coulddown = value;
        }
    }

    virtual public string SkillName{
        get{
            return skillName;
        }
        set {
            skillName = value;
        }
    }

    virtual public string SkillDesc{
        get{
            return skillDesc;
        }
        set {
            skillDesc = value;
        }
    }


}
