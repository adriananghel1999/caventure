﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuSong : MonoBehaviour
{
    void OnEnable(){
        Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.menuSong;
        Music.Instance.gameObject.GetComponent<AudioSource>().Play();
        
    }
}
