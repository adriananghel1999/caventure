﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public static Sound Instance;

    public AudioClip damage, heal, death, defense, chestOpen, torchUp, enemyTouch, startGame, winBoss, winNormal, monsterDead;

    void Awake()
        {

            if (Instance == null)
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

        }
    
}
