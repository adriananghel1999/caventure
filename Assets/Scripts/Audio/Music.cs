﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public static Music Instance;

    public AudioClip[] ambient;
    public AudioClip normalFight, eliteFight, bossFight, deadSong, menuSong;


    public int randomNumber()
    {
        int number = Random.Range(0,ambient.Length);
        return number;
    }

    void Awake()
        {

            if (Instance == null)
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

        }
}
