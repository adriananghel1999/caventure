﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleSystem : MonoBehaviour
{
    // Panel de Victoria/Derrota

    GameObject BattleEnd, WinLoseText, RewardPanel, RewardImages, ScoreText, btnEndRun;
    bool victoria;
    public GameObject IA, NewGame;
    public List<GameObject> objetosLoot;

    CompartirPuntuacion compartir;

    // Inventario

    private Inventory inventory;

    // Preparando las variables de los enemigos y heroes
    GlobalControl globalControl;
    FightLoad fightLoad;
    public List<GameObject> Heroes, Enemy;

    // Vida
    GameObject hero1health, hero2health, hero3health, hero4health, enemy1health, enemy2health, enemy3health, enemy4health;
    public List<GameObject> HeroesHealth, EnemyHealth;

    // Paneles de abajo
    // Inicial

    GameObject CuadroAbajo, PanelInicial, btnAttack, btnSpecial, btnDefense;

    // Target

    GameObject PanelTarget, target1, target2, target3, target4;
    List<GameObject> botonesTarget;

    // Special

    GameObject PanelSpecial, spell1, spell2, spell3, spell4;
    List<GameObject> botonesSpell;

    public Skill spellSelected;
    public List<GameObject> spellTargets;

    // Variables para el combate

    public int turno = 0;
    public int turnoParticipante = 0;
    public List<GameObject> participantes;
    public enum acciones { None, Attack, Spell };
    public acciones accion;

    void Start()
    {
        // Compartir resultados
        compartir = gameObject.GetComponent<CompartirPuntuacion>();

        // Inventario

        inventory = GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel").gameObject.GetComponent<Inventory>();

        // Vida

        HeroesHealth = new List<GameObject>();
        EnemyHealth = new List<GameObject>();

        hero1health = GameObject.Find("h1health");
        hero2health = GameObject.Find("h2health");
        hero3health = GameObject.Find("h3health");
        hero4health = GameObject.Find("h4health");
        enemy1health = GameObject.Find("E1health");
        enemy2health = GameObject.Find("E2health");
        enemy3health = GameObject.Find("E3health");
        enemy4health = GameObject.Find("E4health");

        HeroesHealth.Add(hero1health);
        HeroesHealth.Add(hero2health);
        HeroesHealth.Add(hero3health);
        HeroesHealth.Add(hero4health);
        EnemyHealth.Add(enemy1health);
        EnemyHealth.Add(enemy2health);
        EnemyHealth.Add(enemy3health);
        EnemyHealth.Add(enemy4health);

        // Paneles victoria/derrota

        BattleEnd = GameObject.Find("BattleEnd");
        WinLoseText = GameObject.Find("WinLoseText");
        RewardPanel = GameObject.Find("RewardPanel");
        RewardImages = GameObject.Find("Reward");
        ScoreText = GameObject.Find("ScoreText");
        btnEndRun = GameObject.Find("btnEndRun");

        objetosLoot = new List<GameObject>();

        // Paneles y botones iniciales

        CuadroAbajo = GameObject.Find("CuadroAbajo");

        PanelInicial = GameObject.Find("PanelInicial");
        btnAttack = GameObject.Find("btnAttack");
        btnSpecial = GameObject.Find("btnSpecial");
        btnDefense = GameObject.Find("btnDefense");

        // Special

        PanelSpecial = GameObject.Find("PanelSpecial");

        spell1 = GameObject.Find("Spell1");
        spell2 = GameObject.Find("Spell2");
        spell3 = GameObject.Find("Spell3");
        spell4 = GameObject.Find("Spell4");

        spellTargets = new List<GameObject>();

        botonesSpell = new List<GameObject>();
        botonesSpell.Add(spell1);
        botonesSpell.Add(spell2);
        botonesSpell.Add(spell3);
        botonesSpell.Add(spell4);

        // Target

        PanelTarget = GameObject.Find("PanelTarget");

        target1 = GameObject.Find("Target1");
        target2 = GameObject.Find("Target2");
        target3 = GameObject.Find("Target3");
        target4 = GameObject.Find("Target4");

        botonesTarget = new List<GameObject>();
        botonesTarget.Add(target1);
        botonesTarget.Add(target2);
        botonesTarget.Add(target3);
        botonesTarget.Add(target4);

        PanelTarget.SetActive(false);
        PanelSpecial.SetActive(false);

        // Coger todo lo necesario

        globalControl = GameObject.Find("GlobalControl").GetComponent<GlobalControl>();
        fightLoad = GameObject.Find("Fight").GetComponent<FightLoad>();

        Heroes = fightLoad.getHeroesUI();
        Enemy = fightLoad.getEnemys();

        // Quitando cosas null
        for (int i = Heroes.Count - 1; i >= 0; i--)
        {
            if (Heroes[i].GetComponent<CreatureData>().uniqueCreature != null)
            {
                participantes.Add(Heroes[i]);

            }
            else
            {
                Heroes.RemoveAt(i);
                HeroesHealth.RemoveAt(i);
            }
        }

        for (int i = Enemy.Count - 1; i >= 0; i--)
        {
            if (Enemy[i].GetComponent<CreatureData>().uniqueCreature != null)
            {
                participantes.Add(Enemy[i]);
                Enemy enemy = (Enemy)Enemy[i].GetComponent<CreatureData>().uniqueCreature;
                int objetoLoot = Random.Range(0, enemy.Loot.Length);
                objetosLoot.Add(enemy.Loot[objetoLoot]);
            }
            else
            {
                Enemy.RemoveAt(i);
                EnemyHealth.RemoveAt(i);
            }
        }

        participantes.Sort((IComparer<GameObject>)new sortDE());
        participantes.Reverse();

        participantes[turnoParticipante].GetComponent<Animator>().SetBool("turnOn", true);
        updateHealth();
        if (participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
        {

            // Los heroes han muerto, pierdes/derrota
            if (Heroes.Count == 0)
            {
                StartCoroutine(lose());
            }
            else
            {
                CuadroAbajo.SetActive(false);
                StartCoroutine(turnoEnemigo(4));



            }

        }

    }

    // Sirve para ordenar de menor a mayor la destreza
    private class sortDE : IComparer<GameObject>
    {
        int IComparer<GameObject>.Compare(GameObject _objA, GameObject _objB)
        {
            int t1;
            int t2;
            //t1
            if (_objA.GetComponent<CreatureData>().uniqueCreature as Hero is Hero)
            {
                Hero hero = (Hero)_objA.GetComponent<CreatureData>().uniqueCreature;
                t1 = hero.DE;

            }
            else if (_objA.GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
            {
                Enemy enemy = (Enemy)_objA.GetComponent<CreatureData>().uniqueCreature;
                t1 = enemy.DE;
            }
            else
            {
                t1 = _objA.GetComponent<CreatureData>().uniqueCreature.DE;
            }

            //t2
            if (_objB.GetComponent<CreatureData>().uniqueCreature as Hero is Hero)
            {
                Hero hero = (Hero)_objB.GetComponent<CreatureData>().uniqueCreature;
                t2 = hero.DE;

            }
            else if (_objB.GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
            {
                Enemy enemy = (Enemy)_objB.GetComponent<CreatureData>().uniqueCreature;
                t2 = enemy.DE;
            }
            else
            {
                t2 = _objB.GetComponent<CreatureData>().uniqueCreature.DE;
            }

            return t1.CompareTo(t2);
        }

    }

    // Sirve para ordenar de menor a mayor la vida
    private class sortHP : IComparer<GameObject>
    {
        int IComparer<GameObject>.Compare(GameObject _objA, GameObject _objB)
        {
            int t1;
            int t2;
            //t1
            if (_objA.GetComponent<CreatureData>().uniqueCreature as Hero is Hero)
            {
                Hero hero = (Hero)_objA.GetComponent<CreatureData>().uniqueCreature;
                t1 = hero.HP;

            }
            else if (_objA.GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
            {
                Enemy enemy = (Enemy)_objA.GetComponent<CreatureData>().uniqueCreature;
                t1 = enemy.HP;
            }
            else
            {
                t1 = _objA.GetComponent<CreatureData>().uniqueCreature.HP;
            }

            //t2
            if (_objB.GetComponent<CreatureData>().uniqueCreature as Hero is Hero)
            {
                Hero hero = (Hero)_objB.GetComponent<CreatureData>().uniqueCreature;
                t2 = hero.HP;

            }
            else if (_objB.GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
            {
                Enemy enemy = (Enemy)_objB.GetComponent<CreatureData>().uniqueCreature;
                t2 = enemy.HP;
            }
            else
            {
                t2 = _objB.GetComponent<CreatureData>().uniqueCreature.HP;
            }

            return t1.CompareTo(t2);
        }

    }

    public void btnAtacar()
    {
        PanelInicial.SetActive(false);
        PanelTarget.SetActive(true);
        accion = acciones.Attack;
        mostrarEnemigos();

    }

    public void btnEspecial()
    {
        PanelInicial.SetActive(false);
        PanelSpecial.SetActive(true);
        accion = acciones.Spell;
        mostrarHechizos();

    }

    public void btnDefender()
    {
        participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature.Status = Creature.status.Defend;
        participantes[turnoParticipante].GetComponent<Animator>().SetBool("defensive", true);
        Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.defense;
        Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
        siguienteParticipante();
    }

    public void mostrarAliados()
    {
        for (int i = 0; i < Heroes.Count; i++)
        {

            botonesTarget[i].GetComponentInChildren<Text>().text = Heroes[i].GetComponent<CreatureData>().uniqueCreature.nameCreature;
            botonesTarget[i].GetComponent<CreatureAndGameObject>().GOselected = Heroes[i];
        }

        for (int i = 0; i < botonesTarget.Count; i++)
        {
            if (botonesTarget[i].GetComponentInChildren<Text>().text == "Target")
            {
                botonesTarget[i].SetActive(false);
            }
        }
    }

    public void mostrarEnemigos()
    {

        for (int i = 0; i < Enemy.Count; i++)
        {

            botonesTarget[i].GetComponentInChildren<Text>().text = Enemy[i].GetComponent<CreatureData>().uniqueCreature.nameCreature;
            botonesTarget[i].GetComponent<CreatureAndGameObject>().GOselected = Enemy[i];
        }

        for (int i = 0; i < botonesTarget.Count; i++)
        {
            if (botonesTarget[i].GetComponentInChildren<Text>().text == "Target")
            {
                botonesTarget[i].SetActive(false);
            }
        }

    }

    public void mostrarHechizos()
    {
        for (int i = 0; i < participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature.skills.Count; i++)
        {
            if (participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature.skills[i] != null)
            {
                botonesSpell[i].GetComponentInChildren<Text>().text = participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature.skills[i].SkillName;
                botonesSpell[i].GetComponent<SpellSelected>().SPELLSelected = participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature.skills[i];
                // Enfriamiento hechizo
                if (botonesSpell[i].GetComponent<SpellSelected>().SPELLSelected.coulddownFinish > turno)
                {
                    botonesSpell[i].GetComponent<Button>().interactable = false;
                    ColorBlock cb = botonesSpell[i].GetComponent<Button>().colors;
                    cb.normalColor = Color.black;
                }
                else
                {
                    botonesSpell[i].GetComponent<Button>().interactable = true;
                    ColorBlock cb = botonesSpell[i].GetComponent<Button>().colors;
                    cb.normalColor = Color.white;
                }
            }

        }

        for (int i = 0; i < botonesSpell.Count; i++)
        {
            if (botonesSpell[i].GetComponentInChildren<Text>().text == "Spell")
            {
                botonesSpell[i].SetActive(false);
            }
        }
    }

    public void volver()
    {
        if (PanelTarget.activeInHierarchy)
        {

            for (int i = 0; i < botonesTarget.Count; i++)
            {
                botonesTarget[i].GetComponentInChildren<Text>().text = "Target";
                botonesTarget[i].GetComponent<CreatureAndGameObject>().GOselected = null;
                ColorBlock cb = botonesTarget[i].GetComponent<Button>().colors;
                cb.normalColor = Color.white;
                botonesTarget[i].GetComponent<Button>().colors = cb;
                botonesTarget[i].SetActive(true);

            }
            PanelTarget.SetActive(false);
            PanelInicial.SetActive(true);
            spellSelected = null;
            spellTargets.Clear();
            accion = acciones.None;



        }
        else if (PanelSpecial.activeInHierarchy)
        {
            for (int i = 0; i < botonesSpell.Count; i++)
            {
                botonesSpell[i].GetComponentInChildren<Text>().text = "Spell";
                botonesSpell[i].GetComponent<SpellSelected>().SPELLSelected = null;
                botonesSpell[i].SetActive(true);

            }
            PanelInicial.SetActive(true);
            PanelSpecial.SetActive(false);
            accion = acciones.None;
        }

    }

    public void pulsarTarget(Button target)
    {
        if (accion == acciones.Attack)
        {
            Enemy enemy = (Enemy)target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<CreatureData>().uniqueCreature;
            Hero hero = (Hero)participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature;

            if (enemy.Status == Creature.status.Defend)
            {
                target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Animator>().SetBool("damaged", true);
                target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Animator>().Play("damaged", -1, 0f);
                enemy.CurrentHP -= (int)(hero.damage / 1.5f);

            }
            else
            {
                target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Animator>().SetBool("damaged", true);
                target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Animator>().Play("damaged", -1, 0f);
                enemy.CurrentHP -= hero.damage;
            }
            target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Animator>().SetBool("damaged", false);
            Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.damage;
            Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
            dejarTarget(target);
            volver();
            siguienteParticipante();
        }
        else if (accion == acciones.Spell)
        {

            if (spellTargets.Contains(target.GetComponent<CreatureAndGameObject>().GOselected))
            {
                spellTargets.Remove(target.GetComponent<CreatureAndGameObject>().GOselected);
                ColorBlock cb = target.colors;
                cb.normalColor = Color.white;
                target.colors = cb;
                GameObject myEventSystem = GameObject.Find("EventSystem");
                myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
            }
            else
            {
                spellTargets.Add(target.GetComponent<CreatureAndGameObject>().GOselected);
                ColorBlock cb = target.colors;
                cb.normalColor = Color.grey;
                target.colors = cb;
            }
            // Aqui empieza la magia, si hay menos targets de los maximos posibles, se ejecuta cuando llega al maximo de enemigos en combate
            if (spellTargets.Count == spellSelected.targets || spellSelected as Damage is Damage && spellTargets.Count == Enemy.Count || spellSelected as Heal is Heal && spellTargets.Count == Heroes.Count)
            {
                dejarTarget(target);
                spellSelected.coulddownFinish = spellSelected.coulddown + turno;

                if (spellSelected as Damage is Damage)
                {
                    foreach (GameObject Target in spellTargets)
                    {
                        Enemy enemy = (Enemy)Target.GetComponent<CreatureData>().uniqueCreature;
                        Hero hero = (Hero)participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature;
                        Damage spell = (Damage)spellSelected;

                        if (enemy.Status == Creature.status.Defend)
                        {
                            Target.GetComponent<Animator>().SetBool("damaged", true);
                            Target.GetComponent<Animator>().Play("damaged", -1, 0f);
                            enemy.CurrentHP -= (int)(spell.damage(hero) / 1.5f);

                        }
                        else
                        {
                            Target.GetComponent<Animator>().SetBool("damaged", true);
                            Target.GetComponent<Animator>().Play("damaged", -1, 0f);
                            enemy.CurrentHP -= (int)(spell.damage(hero) / 1.5f);
                        }
                        Target.GetComponent<Animator>().SetBool("damaged", false);
                        Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.damage;
                        Sound.Instance.gameObject.GetComponent<AudioSource>().Play();

                    }

                }
                else if (spellSelected as Heal is Heal)
                {
                    foreach (GameObject Target in spellTargets)
                    {
                        Hero heroTarget = (Hero)Target.GetComponent<CreatureData>().uniqueCreature;
                        Hero hero = (Hero)participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature;
                        Heal spell = (Heal)spellSelected;

                        Target.GetComponent<Animator>().SetBool("healed", true);
                        Target.GetComponent<Animator>().Play("healed", -1, 0f);
                        heroTarget.CurrentHP += spell.Healing(hero);
                        if (heroTarget.CurrentHP > heroTarget.HP)
                        {
                            heroTarget.CurrentHP = heroTarget.HP;
                        }
                        Target.GetComponent<Animator>().SetBool("healed", false);
                        Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.heal;
                        Sound.Instance.gameObject.GetComponent<AudioSource>().Play();

                    }
                }
                volver();
                siguienteParticipante();
            }

        }


    }

    public void pulsarSpell(Button spell)
    {
        spellSelected = spell.GetComponent<SpellSelected>().SPELLSelected;
        for (int i = 0; i < botonesSpell.Count; i++)
        {
            botonesSpell[i].GetComponentInChildren<Text>().text = "Spell";
            botonesSpell[i].GetComponent<SpellSelected>().SPELLSelected = null;
            botonesSpell[i].SetActive(true);

        }
        PanelSpecial.SetActive(false);
        PanelTarget.SetActive(true);

        if (spellSelected as Damage is Damage)
        {
            mostrarEnemigos();
        }
        else if (spellSelected as Heal is Heal)
        {
            mostrarAliados();
        }

    }

    public void mostrarTarget(Button target)
    {
        target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Outline>().enabled = true;
    }

    public void dejarTarget(Button target)
    {
        target.GetComponent<CreatureAndGameObject>().GOselected.GetComponent<Outline>().enabled = false;
    }


    public void siguienteParticipante()
    {
        updateHealth();
        participantes[turnoParticipante].GetComponent<Animator>().SetBool("turnOn", false);
        accion = acciones.None;

        // Comprobando si alguien esta muerto
        whoDead(participantes[turnoParticipante]);

        if (turnoParticipante >= participantes.Count - 1)
        {
            // Quitar el estado de defender despues de un turno
            for (int i = 0; i < participantes.Count - 1; i++)
            {
                participantes[i].GetComponent<CreatureData>().uniqueCreature.Status = Creature.status.Alive;
                participantes[i].GetComponent<Animator>().SetBool("defensive", false);
            }

            turnoParticipante = 0;
            turno++;
        }
        else
        {
            turnoParticipante++;
        }

        // Comprobando si es el turno del enemigo

        if (participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
        {

            // Los heroes han muerto, pierdes/derrota
            if (Heroes.Count == 0)
            {
                StartCoroutine(lose());
            }
            else
            {
                CuadroAbajo.SetActive(false);
                StartCoroutine(turnoEnemigo(4));



            }

        }
        else
        {
            participantes[turnoParticipante].GetComponent<Animator>().SetBool("turnOn", true);
        }
    }

    void whoDead(GameObject participanteActual)
    {
        for (int i = participantes.Count - 1; i >= 0; i--)
        {

            if (participantes[i].GetComponent<CreatureData>().uniqueCreature as Hero is Hero)
            {
                Hero hero = (Hero)participantes[i].GetComponent<CreatureData>().uniqueCreature;
                //Si el heroe esta muerto quitarlo de participantes y lista de heroes
                if (hero.CurrentHP <= 0)
                {
                    Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.death;
                    Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
                    hero.Status = Creature.status.Dead;

                    for (int f = Heroes.Count - 1; f >= 0; f--)
                    {
                        if (Heroes[f].GetComponent<CreatureData>().uniqueCreature.Status == Creature.status.Dead)
                        {
                            HeroesHealth.RemoveAt(f);
                            GlobalControl.Instance.heroParty.RemoveAt(f);
                            Destroy(GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Equipment").gameObject.transform.GetChild(f).gameObject);
                            Destroy(GlobalInventory.Instance.gameObject.transform.Find("InventarioPanel/Spells").gameObject.transform.GetChild(f).gameObject);
                            Heroes.RemoveAt(f);

                        }

                    }
                    // Esto sirve para que no se pase de participante en caso de que un heroe haya matado a un enemigo con destreza mayor que el heroe
                    Enemy enemy = (Enemy)participanteActual.GetComponent<CreatureData>().uniqueCreature;
                    if (enemy.DE < hero.DE)
                    {
                        turnoParticipante--;
                    }

                    participantes[i].GetComponent<Animator>().SetBool("dead", true);
                    CuadroAbajo.SetActive(false);
                    StartCoroutine(removerParticipante(2f, participantes[i]));
                    participantes.RemoveAt(i);

                    // Los heroes han muerto, pierdes/derrota
                    if (Heroes.Count == 0)
                    {
                        StartCoroutine(lose());
                    }
                }

            }
            else if (participantes[i].GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
            {

                Enemy enemy = (Enemy)participantes[i].GetComponent<CreatureData>().uniqueCreature;
                // Si el enemigo esta muerto quitarlo de participantes y lista de enemigos

                // Esto es para puntuacion y logros
                if (enemy.CurrentHP <= 0)
                {
                    if (enemy.Type == global::Enemy.enemyType.Normal)
                    {
                        GlobalControl.Instance.puntuacion += 10;
                    }
                    else if (enemy.Type == global::Enemy.enemyType.Elite)
                    {
                        GlobalControl.Instance.puntuacion += 50;
                        GlobalAchivements.Instance.eliteKilled++;

                        if (GlobalAchivements.Instance.eliteKilled == 20)
                        {
                            GlobalAchivements.Instance.achivementsScriptableObject[2].itIsDone = true;
                        }
                    }
                    else if (enemy.Type == global::Enemy.enemyType.Boss)
                    {
                        GlobalControl.Instance.puntuacion += 100;
                        GlobalAchivements.Instance.bossKilled++;

                        if (GlobalAchivements.Instance.bossKilled == 10)
                        {
                            GlobalAchivements.Instance.achivementsScriptableObject[1].itIsDone = true;
                        }
                    }
                    Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.monsterDead;
                    Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
                    enemy.Status = Creature.status.Dead;

                    for (int f = Enemy.Count - 1; f >= 0; f--)
                    {
                        if (Enemy[f].GetComponent<CreatureData>().uniqueCreature.Status == Creature.status.Dead)
                        {
                            EnemyHealth.RemoveAt(f);
                            Enemy.RemoveAt(f);

                        }

                    }

                    // Esto sirve para que no se pase de participante en caso de que un enemigo haya matado a un heroe con destreza mayor que el enemigo
                    Hero hero = (Hero)participanteActual.GetComponent<CreatureData>().uniqueCreature;
                    if (hero.DE < enemy.DE)
                    {
                        turnoParticipante--;
                    }

                    participantes[i].GetComponent<Animator>().SetBool("dead", true);
                    CuadroAbajo.SetActive(false);
                    StartCoroutine(removerParticipante(2f, participantes[i]));
                    participantes.RemoveAt(i);
                }

            }
        }

        // Victoria, el jugador gana loot
        if (Enemy.Count == 0)
        {

            StartCoroutine(win());

        }

    }

    // Necesario para que la animacion de muerte se produzca
    IEnumerator removerParticipante(float time, GameObject participante)
    {

        yield return new WaitForSeconds(time);

        if (participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature as Enemy is Enemy)
        {
        }
        else
        {
            CuadroAbajo.SetActive(true);
        }

        participante.SetActive(false);

    }

    //Enemigo ataca al heroe con mas vida MAXIMA
    IEnumerator turnoEnemigo(float time)
    {
        participantes[turnoParticipante].GetComponent<Animator>().SetBool("turnOn", true);
        yield return new WaitForSeconds(time);

        List<GameObject> HeroesHP;
        HeroesHP = new List<GameObject>(Heroes);
        HeroesHP.Sort((IComparer<GameObject>)new sortHP());
        HeroesHP.Reverse();

        Enemy enemy = (Enemy)participantes[turnoParticipante].GetComponent<CreatureData>().uniqueCreature;
        Hero hero = null;

        //Decidir si el enemigo va ha atacar aleatoriamente o al heroe con mas vida
        int rand = Random.Range(0, 5);
        int randHero = Random.Range(0, HeroesHP.Count - 1);

        // 0,1,2,3 heroe con mas vida 4,5 aleatorio
        if (rand <= 3)
        {
            hero = (Hero)HeroesHP[0].GetComponent<CreatureData>().uniqueCreature;
        }
        else if (rand >= 4)
        {
            hero = (Hero)HeroesHP[randHero].GetComponent<CreatureData>().uniqueCreature;
        }

        if (hero.Status == Creature.status.Defend)
        {
            // 0,1,2,3 heroe con mas vida 4,5 aleatorio
            if (rand <= 2)
            {
                HeroesHP[0].GetComponent<Animator>().SetBool("damaged", true);
                HeroesHP[0].GetComponent<Animator>().Play("damaged", -1, 0f);
            }
            else if (rand >= 3)
            {
                HeroesHP[randHero].GetComponent<Animator>().SetBool("damaged", true);
                HeroesHP[randHero].GetComponent<Animator>().Play("damaged", -1, 0f);
            }

            hero.CurrentHP -= (int)(enemy.damage / 1.5f);

        }
        else
        {
            // 0,1,2 heroe con mas vida 3,4,5 aleatorio
            if (rand <= 2)
            {
                HeroesHP[0].GetComponent<Animator>().SetBool("damaged", true);
                HeroesHP[0].GetComponent<Animator>().Play("damaged", -1, 0f);
            }
            else if (rand >= 3)
            {
                HeroesHP[randHero].GetComponent<Animator>().SetBool("damaged", true);
                HeroesHP[randHero].GetComponent<Animator>().Play("damaged", -1, 0f);
            }

            hero.CurrentHP -= enemy.damage;
        }
        Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.damage;
        Sound.Instance.gameObject.GetComponent<AudioSource>().Play();
        HeroesHP[0].GetComponent<Animator>().SetBool("damaged", false);
        participantes[turnoParticipante].GetComponent<Animator>().SetBool("turnOn", false);

        participantes[turnoParticipante].GetComponent<Animator>().SetBool("turnOn", true);
        CuadroAbajo.SetActive(true);
        siguienteParticipante();
    }

    // Finalizar combate
    public void finalizarCombate()
    {
        GlobalControl.Instance.batallaActiva = false;

        GlobalControl.Instance.batallaElite = false;

        // Poniendo los couldowns a 0

        for (int i = Heroes.Count - 1; i >= 0; i--)
        {

            if (Heroes[i].GetComponent<CreatureData>().uniqueCreature.Skills.Count != 0)
            {
                for (int e = Heroes[i].GetComponent<CreatureData>().uniqueCreature.Skills.Count - 1; e >= 0; e--)
                {
                    Heroes[i].GetComponent<CreatureData>().uniqueCreature.Skills[e].coulddownFinish = 0;

                }
            }
            else
            {

            }
        }

        if (victoria)
        {
            // Metiendo el loot en el inventario
            for (int f = 0; f <= objetosLoot.Count - 1; f++)
            {
                for (int i = 0; i < inventory.slots.Length; i++)
                {
                    if (inventory.isFull[i] == false)
                    {
                        inventory.isFull[i] = true;
                        Instantiate(objetosLoot[f], inventory.slots[i].transform, false);
                        break;
                    }
                }
            }
            Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.ambient[Music.Instance.randomNumber()];
            Music.Instance.gameObject.GetComponent<AudioSource>().Play();

            if (GlobalControl.Instance.batallaBoss == true)
            {
                GlobalControl.Instance.batallaBoss = false;
                GlobalInventory.Instance.gameObject.SetActive(true);
                GlobalMenu.Instance.gameObject.SetActive(true);
                SceneManager.UnloadSceneAsync(2);
                SceneManager.LoadSceneAsync(1);
                
            }
            else
            {
                GlobalControl.Instance.batallaBoss = false;
                SceneManager.UnloadSceneAsync(2);
            }


        }
        else
        {
            Destroy(GlobalInventory.Instance.gameObject);
            Destroy(GlobalControl.Instance.gameObject);
            Destroy(GlobalMenu.Instance.gameObject);
            SceneManager.LoadScene(0);
            SceneManager.UnloadSceneAsync(1);
            SceneManager.UnloadSceneAsync(2);
        }

    }

    public void finalizarRun()
    {
        // Aqui se comprobara logros conseguidos y en caso de que se pueda compartir resultados, compartir resultados, ademas de reiniciar los valores en caso de que se quiera hacer otra run

        GlobalAchivements.Instance.achivementsScriptableObject[0].itIsDone = true;

        if (GlobalControl.Instance.dificultad == 2)
        {
            GlobalAchivements.Instance.achivementsScriptableObject[3].itIsDone = true;
        }

        if (GlobalControl.Instance.dificultad == 3)
        {
            GlobalAchivements.Instance.achivementsScriptableObject[4].itIsDone = true;
        }

        GlobalAchivements.Instance.saveProgress();

        if (GlobalOptions.Instance.shareScore == true)
        {
            compartir.tryCompartirPuntuacion();
        }

        // ir al menu
        Destroy(GlobalInventory.Instance.gameObject);
        Destroy(GlobalControl.Instance.gameObject);
        Destroy(GlobalMenu.Instance.gameObject);
        SceneManager.LoadScene(0);
        SceneManager.UnloadSceneAsync(1);
        SceneManager.UnloadSceneAsync(2);
    }

    void updateHealth()
    {
        for (int i = 0; i < EnemyHealth.Count; i++)
        {
            EnemyHealth[i].GetComponent<Text>().text = Enemy[i].GetComponent<CreatureData>().uniqueCreature.CurrentHP.ToString() + "/" + Enemy[i].GetComponent<CreatureData>().uniqueCreature.HP.ToString();

        }

        for (int i = 0; i < HeroesHealth.Count; i++)
        {
            HeroesHealth[i].GetComponent<Text>().text = GlobalControl.Instance.heroParty[i].CurrentHP.ToString() + "/" + GlobalControl.Instance.heroParty[i].HP.ToString();

        }
    }

    IEnumerator lose()
    {
        yield return new WaitForSeconds(2f);
        victoria = false;
        BattleEnd.GetComponent<CanvasGroup>().alpha = 1;
        BattleEnd.GetComponent<CanvasGroup>().blocksRaycasts = true;
        RewardPanel.GetComponent<CanvasGroup>().alpha = 0;
        RewardPanel.GetComponent<CanvasGroup>().blocksRaycasts = false;
        WinLoseText.GetComponent<Text>().text = "Defeat";
        WinLoseText.GetComponent<Text>().color = Color.red;
        Music.Instance.gameObject.GetComponent<AudioSource>().Stop();
        Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.deadSong;
        Music.Instance.gameObject.GetComponent<AudioSource>().Play();
    }

    IEnumerator win()
    {
        yield return new WaitForSeconds(2f);
        victoria = true;

        for (int i = 0; i <= objetosLoot.Count - 1; i++)
        {
            RewardImages.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = objetosLoot[i].GetComponent<Image>().sprite;


        }

        for (int i = RewardImages.transform.childCount - 1; i >= 0; i--)
        {
            if (RewardImages.transform.GetChild(i).gameObject.GetComponent<Image>().sprite == null)
            {
                Destroy(RewardImages.transform.GetChild(i).gameObject);
            }
        }

        BattleEnd.GetComponent<CanvasGroup>().alpha = 1;
        BattleEnd.GetComponent<CanvasGroup>().blocksRaycasts = true;
        WinLoseText.GetComponent<Text>().text = "Victory";
        WinLoseText.GetComponent<Text>().color = Color.green;

        if (GlobalControl.Instance.batallaBoss == true)
        {
            btnEndRun.GetComponent<CanvasGroup>().alpha = 1;
            btnEndRun.GetComponent<CanvasGroup>().interactable = true;
            GlobalControl.Instance.puntuacion += 200;
            GlobalControl.Instance.puntuacion = GlobalControl.Instance.puntuacion * GlobalControl.Instance.heroParty.Count;
            Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.winBoss;
        }
        else
        {
            Sound.Instance.gameObject.GetComponent<AudioSource>().clip = Sound.Instance.winNormal;
        }
        Sound.Instance.gameObject.GetComponent<AudioSource>().Play();

        ScoreText.GetComponent<Text>().text = "Current score: " + GlobalControl.Instance.puntuacion;
        Music.Instance.gameObject.GetComponent<AudioSource>().Stop();
    }




}
