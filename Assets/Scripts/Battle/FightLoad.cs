﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightLoad : MonoBehaviour
{
    // Arrays cantidad enemigos

    public int[] cantidadEnemigosFacil;
    public int[] cantidadEnemigosNormal;
    public int[] cantidadEnemigosDificil;
    public int[] cantidadEnemigosMuyDificil;
    public int[] cantidadEnemigos;
    public int[] cantidadEnemigosBossNoMuyDificil;

    // Posibles enemigos

    public GameObject[] posiblesEnemigosFacil;
    public GameObject[] posiblesEnemigosNormal;
    public GameObject[] posiblesEnemigosDificil;
    public GameObject[] posiblesEnemigosMuyDificil;

    // Boses

    public GameObject[] posiblesBoses;

    public GameObject[] posiblesElites;
    public GameObject[] posiblesEnemigos;

    // Inventario
    GameObject hero1, hero2, hero3, hero4, enemy1, enemy2, enemy3, enemy4;
    public List<GameObject> Heroes, Enemy;
    List<GameObject> HeroesHealth, EnemyHealth;

    // Start is called before the first frame update
    void Start()
    {
        cantidadEnemigosBossNoMuyDificil = new int[1]{1};

        Heroes = new List<GameObject>();
        Enemy = new List<GameObject>();

        hero1 = GameObject.Find("h1");
        hero2 = GameObject.Find("h2");
        hero3 = GameObject.Find("h3");
        hero4 = GameObject.Find("h4");
        enemy1 = GameObject.Find("E1");
        enemy2 = GameObject.Find("E2");
        enemy3 = GameObject.Find("E3");
        enemy4 = GameObject.Find("E4");

        Heroes.Add(hero1);
        Heroes.Add(hero2);
        Heroes.Add(hero3);
        Heroes.Add(hero4);
        Enemy.Add(enemy1);
        Enemy.Add(enemy2);
        Enemy.Add(enemy3);
        Enemy.Add(enemy4);

        // Contra cuantos enemigos se va a enfrentar el jugador segun la dificultad y contra cuales
        Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.normalFight;

        if (GlobalControl.Instance.dificultad == 0 && GlobalControl.Instance.batallaBoss == false)
        {
            cantidadEnemigos = cantidadEnemigosFacil;
            posiblesEnemigos = posiblesEnemigosFacil;
        }
        else if (GlobalControl.Instance.dificultad == 1 && GlobalControl.Instance.batallaBoss == false)
        {
            cantidadEnemigos = cantidadEnemigosNormal;
            posiblesEnemigos = posiblesEnemigosNormal;
        }
        else if (GlobalControl.Instance.dificultad == 2 && GlobalControl.Instance.batallaBoss == false)
        {
            cantidadEnemigos = cantidadEnemigosDificil;
            posiblesEnemigos = posiblesEnemigosDificil;
        }
        else if (GlobalControl.Instance.dificultad == 3 && GlobalControl.Instance.batallaBoss == false)
        {
            cantidadEnemigos = cantidadEnemigosMuyDificil;
            posiblesEnemigos = posiblesEnemigosMuyDificil;
        }

        if (GlobalControl.Instance.batallaBoss == true && GlobalControl.Instance.dificultad != 3)
        {
            Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.bossFight;
            posiblesEnemigos = posiblesBoses;
            cantidadEnemigos = cantidadEnemigosBossNoMuyDificil;    
            
        } else if (GlobalControl.Instance.batallaBoss == true && GlobalControl.Instance.dificultad == 3) {
            Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.bossFight;
            posiblesEnemigos = posiblesBoses;
            cantidadEnemigos = cantidadEnemigosFacil;
        }
        if (GlobalControl.Instance.batallaElite == true)
        {
            Music.Instance.gameObject.GetComponent<AudioSource>().clip = Music.Instance.eliteFight;
            posiblesEnemigos = posiblesElites;
        }

        int randCantidad = Random.Range(0, cantidadEnemigos.Length);

        Music.Instance.gameObject.GetComponent<AudioSource>().Play();

        for (int i = 0; i < cantidadEnemigos[randCantidad]; i++)
        {
            int randEnemigos = Random.Range(0, posiblesEnemigos.Length);
            Enemy[i].GetComponent<Image>().color = posiblesEnemigos[randEnemigos].GetComponent<Image>().color;
            Enemy[i].GetComponent<Image>().sprite = posiblesEnemigos[randEnemigos].GetComponent<Image>().sprite;
            Enemy[i].GetComponent<Animator>().runtimeAnimatorController = posiblesEnemigos[randEnemigos].GetComponent<Animator>().runtimeAnimatorController;
            Enemy[i].GetComponent<CreatureData>().creatureData = posiblesEnemigos[randEnemigos].GetComponent<CreatureData>().creatureData;
            Enemy[i].GetComponent<Transform>().localScale = posiblesEnemigos[randEnemigos].GetComponent<Transform>().localScale;
            Enemy[i].GetComponent<CreatureData>().creatureData.currentHp = Enemy[i].GetComponent<CreatureData>().creatureData.HP;
            

        }


        for (int i = 0; i < GlobalControl.Instance.heroParty.Count; i++)
        {
            Heroes[i].GetComponent<Image>().sprite = GlobalControl.Instance.heroParty[i].Icon;
            Heroes[i].GetComponent<CreatureData>().uniqueCreature = GlobalControl.Instance.heroParty[i];

        }

        // Esto sirve para limpiar el escenario de cosas que no estan
        for (int i = 0; i <= 3; i++)
        {
            if (Heroes[i].GetComponent<Image>().sprite == null)
            {
                Destroy(Heroes[i].transform.parent.gameObject);
            }

            if (Enemy[i].GetComponent<Image>().sprite == null)
            {
                Destroy(Enemy[i].gameObject);
            }
        }

    }


    public List<GameObject> getHeroesUI()
    {
        return Heroes;
    }

    public List<GameObject> getEnemys()
    {
        return Enemy;
    }


}
