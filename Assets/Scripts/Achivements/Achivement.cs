﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Achivement", menuName = "Achivement", order = 51)]
[System.Serializable]
public class Achivement : ScriptableObject
{
    public string achivementName;
    public string achivementDesc;
    public bool itIsDone = false;

}
