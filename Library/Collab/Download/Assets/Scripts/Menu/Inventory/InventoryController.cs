﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryController : MonoBehaviour
{
    public Transform selectedItem = null, selectedSlot = null, originalSlot, canvas;

    public Inventory inventario;

    // info
    private CreatureData hero1, hero2, hero3, hero4;

    void Start()
    {

        if (GameObject.Find("hero1img"))
        {
            hero1 = GameObject.Find("hero1img").GetComponent<CreatureData>();
        }

        if (GameObject.Find("hero2img"))
        {
            hero2 = GameObject.Find("hero2img").GetComponent<CreatureData>();
        }

        if (GameObject.Find("hero3img"))
        {
            hero3 = GameObject.Find("hero3img").GetComponent<CreatureData>();
        }

        if (GameObject.Find("hero4img"))
        {
            hero4 = GameObject.Find("hero4img").GetComponent<CreatureData>();
        }


    }

    void Update()
    { // Al pulsa sobre un objeto
        if (Input.GetMouseButtonDown(0) && selectedItem != null)
        {

            if (selectedSlot.CompareTag("SlotInventario"))
            {
                inventario.isFull[selectedItem.parent.GetSiblingIndex()] = false;

            }

            else if (selectedSlot.CompareTag("SlotHero1"))
            {
                inventario.Hero1full[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero1.uniqueCreature;
                hero.setItem(selectedItem.parent.GetSiblingIndex(), null);

            }

            else if (selectedSlot.CompareTag("SlotHero2"))
            {
                inventario.Hero2full[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero2.uniqueCreature;
                hero.setItem(selectedItem.parent.GetSiblingIndex(), null);
            }

            else if (selectedSlot.CompareTag("SlotHero3"))
            {
                inventario.Hero3full[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero3.uniqueCreature;
                hero.setItem(selectedItem.parent.GetSiblingIndex(), null);
            }

            else if (selectedSlot.CompareTag("SlotHero4"))
            {
                inventario.Hero4full[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero4.uniqueCreature;
                hero.setItem(selectedItem.parent.GetSiblingIndex(), null);
            }

            else if (selectedSlot.CompareTag("SpellHero1"))
            {
                inventario.Hero1fullspells[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero1.uniqueCreature;
                hero.setSpell(selectedItem.parent.GetSiblingIndex(), null);
            }

            else if (selectedSlot.CompareTag("SpellHero2"))
            {
                inventario.Hero2fullspells[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero2.uniqueCreature;
                hero.setSpell(selectedItem.parent.GetSiblingIndex(), null);
            }

            else if (selectedSlot.CompareTag("SpellHero3"))
            {
                inventario.Hero3fullspells[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero3.uniqueCreature;
                hero.setSpell(selectedItem.parent.GetSiblingIndex(), null);
            }

            else if (selectedSlot.CompareTag("SpellHero4"))
            {
                inventario.Hero4fullspells[selectedItem.parent.GetSiblingIndex()] = false;
                Hero hero = (Hero)hero4.uniqueCreature;
                hero.setSpell(selectedItem.parent.GetSiblingIndex(), null);
            }

            originalSlot = selectedItem.parent;
            selectedItem.SetParent(canvas);

        }
        // Mientras el boton esta pulsado
        if (Input.GetMouseButton(0) && selectedItem != null)
        {
            selectedItem.position = Input.mousePosition;
        }
        // Al soltar objeto
        else if (Input.GetMouseButtonUp(0) && selectedItem != null)
        {
            // Objeto soltado a la nada o en un espacio ya ocupado o en un sitio donde no debe estar
            if (selectedSlot == null || selectedSlot.childCount >= 1)
            {
                objetoASlotOriginal();
            }
            // Consumible se gasta
            else if (selectedItem.CompareTag("Consumable") && selectedSlot.CompareTag("Hero"))
            {
                Consumable consumable = (Consumable)selectedItem.GetComponent<ItemData>().uniqueItem;
                Hero hero = (Hero)selectedSlot.GetComponent<CreatureData>().uniqueCreature;
                hero.eatConsumable(consumable);
                selectedItem.GetComponent<ItemUI>().destroyItem();

            }
            // Objeto a equipo
            else if (selectedItem.CompareTag("Equipable") && selectedSlot.CompareTag("SlotHero1") || selectedItem.CompareTag("Equipable") && selectedSlot.CompareTag("SlotHero2") || selectedItem.CompareTag("Equipable") && selectedSlot.CompareTag("SlotHero3") || selectedItem.CompareTag("Equipable") && selectedSlot.CompareTag("SlotHero4"))
            {
                selectedItem.SetParent(selectedSlot);

                if (selectedSlot.CompareTag("SlotHero1"))
                {
                    inventario.Hero1full[selectedItem.parent.GetSiblingIndex()] = true;
                    Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
                    Hero hero = (Hero)hero1.uniqueCreature;
                    hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
                }

                else if (selectedSlot.CompareTag("SlotHero2"))
                {
                    inventario.Hero2full[selectedItem.parent.GetSiblingIndex()] = true;
                    Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
                    Hero hero = (Hero)hero2.uniqueCreature;
                    hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
                }

                else if (selectedSlot.CompareTag("SlotHero3"))
                {
                    inventario.Hero3full[selectedItem.parent.GetSiblingIndex()] = true;
                    Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
                    Hero hero = (Hero)hero3.uniqueCreature;
                    hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
                }

                else if (selectedSlot.CompareTag("SlotHero4"))
                {
                    inventario.Hero4full[selectedItem.parent.GetSiblingIndex()] = true;
                    Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
                    Hero hero = (Hero)hero4.uniqueCreature;
                    hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
                }
            }
            // Objeto a spell
            else if (selectedItem.CompareTag("Spell") && selectedSlot.CompareTag("SpellHero1") || selectedItem.CompareTag("Spell") && selectedSlot.CompareTag("SpellHero2") || selectedItem.CompareTag("Spell") && selectedSlot.CompareTag("SpellHero3") || selectedItem.CompareTag("Spell") && selectedSlot.CompareTag("SpellHero4"))
            {
                selectedItem.SetParent(selectedSlot);

                if (selectedSlot.CompareTag("SpellHero1"))
                {
                    inventario.Hero1fullspells[selectedItem.parent.GetSiblingIndex()] = true;
                    Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
                    Hero hero = (Hero)hero1.uniqueCreature;
                    hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
                }

                else if (selectedSlot.CompareTag("SpellHero2"))
                {
                    inventario.Hero2fullspells[selectedItem.parent.GetSiblingIndex()] = true;
                    Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
                    Hero hero = (Hero)hero2.uniqueCreature;
                    hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
                }

                else if (selectedSlot.CompareTag("SpellHero3"))
                {
                    inventario.Hero3fullspells[selectedItem.parent.GetSiblingIndex()] = true;
                    Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
                    Hero hero = (Hero)hero3.uniqueCreature;
                    hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
                }

                else if (selectedSlot.CompareTag("SpellHero4"))
                {
                    inventario.Hero4fullspells[selectedItem.parent.GetSiblingIndex()] = true;
                    Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
                    Hero hero = (Hero)hero4.uniqueCreature;
                    hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
                }
            }
            // Objeto a inventario
            else if (selectedSlot.CompareTag("SlotInventario"))
            {
                selectedItem.SetParent(selectedSlot);
                inventario.isFull[selectedItem.parent.GetSiblingIndex()] = true;
            }
            else
            {
                objetoASlotOriginal();
            }

            selectedItem.localPosition = Vector3.zero;
            selectedItem.GetComponent<CanvasGroup>().blocksRaycasts = true;
            selectedItem = null;
            originalSlot = null;
        }
    }

    public void setSelectedItem(Transform item)
    {
        selectedItem = item;
        selectedItem.GetComponent<CanvasGroup>().blocksRaycasts = false;

    }

    public void setSelectedSlot(Transform slot)
    {
        selectedSlot = slot;
    }

    public void setSelectedSlotNull()
    {
        selectedSlot = null;
    }

    public void objetoASlotOriginal()
    {
        selectedItem.SetParent(originalSlot);
        if (originalSlot.CompareTag("SlotInventario"))
        {
            inventario.isFull[selectedItem.parent.GetSiblingIndex()] = true;

        }

        else if (originalSlot.CompareTag("SlotHero1"))
        {
            inventario.Hero1full[selectedItem.parent.GetSiblingIndex()] = true;
            Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
            Hero hero = (Hero)hero1.uniqueCreature;
            hero.setItem(selectedItem.parent.GetSiblingIndex(), item);

        }

        else if (originalSlot.CompareTag("SlotHero2"))
        {
            inventario.Hero1full[selectedItem.parent.GetSiblingIndex()] = true;
            Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
            Hero hero = (Hero)hero2.uniqueCreature;
            hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
        }

        else if (originalSlot.CompareTag("SlotHero3"))
        {
            inventario.Hero1full[selectedItem.parent.GetSiblingIndex()] = true;
            Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
            Hero hero = (Hero)hero3.uniqueCreature;
            hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
        }

        else if (originalSlot.CompareTag("SlotHero4"))
        {
            inventario.Hero1full[selectedItem.parent.GetSiblingIndex()] = true;
            Item item = selectedItem.GetComponent<ItemData>().uniqueItem;
            Hero hero = (Hero)hero4.uniqueCreature;
            hero.setItem(selectedItem.parent.GetSiblingIndex(), item);
        }

        else if (originalSlot.CompareTag("SpellHero1"))
        {
            inventario.Hero1fullspells[selectedItem.parent.GetSiblingIndex()] = true;
            Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
            Hero hero = (Hero)hero1.uniqueCreature;
            hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
        }

        else if (originalSlot.CompareTag("SpellHero2"))
        {
            inventario.Hero2fullspells[selectedItem.parent.GetSiblingIndex()] = true;
            Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
            Hero hero = (Hero)hero2.uniqueCreature;
            hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
        }

        else if (originalSlot.CompareTag("SpellHero3"))
        {
            inventario.Hero3fullspells[selectedItem.parent.GetSiblingIndex()] = true;
            Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
            Hero hero = (Hero)hero3.uniqueCreature;
            hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
        }

        else if (originalSlot.CompareTag("SpellHero4"))
        {
            inventario.Hero4fullspells[selectedItem.parent.GetSiblingIndex()] = true;
            Skill Skill = selectedItem.GetComponent<SpellData>().uniqueSpell;
            Hero hero = (Hero)hero4.uniqueCreature;
            hero.setSpell(selectedItem.parent.GetSiblingIndex(), Skill);
        }
    }
}
